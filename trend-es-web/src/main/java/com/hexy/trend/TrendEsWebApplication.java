package com.hexy.trend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrendEsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrendEsWebApplication.class, args);
    }

}
