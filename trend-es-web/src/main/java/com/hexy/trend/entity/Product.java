package com.hexy.trend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @Document 在类级别应用，以指示该类是映射到数据库的候选对象。最重要的属性是：
 * indexName：对应索引库名称
 * type：映射类型。如果未设置，则使用小写的类的简单名称。（从版本4.0开始不推荐使用）
 * shards：索引的分片数
 * replicas：索引的副本数
 * refreshIntervall :索引的刷新间隔。用于索引创建。默认值为“ 1s”。
 * indexStoreType: 索引的索引存储类型。用于索引创建。默认值为“ fs”。
 * createIndex: 配置是否在存储库引导中创建索引。默认值为true。
 * versionType: 版本管理的配置。默认值为EXTERNAL。
 */
@Data
@Document(indexName = "product", shards = 1,replicas = 0)
public class Product {
    /**
     * @Id 作用在成员变量，标记一个字段作为id主键
     * @Transient :默认情况下，存储或检索文档时，所有字段都映射到文档，此注释不包括该字段。
     * @PersistenceConstructor: 标记从数据库实例化对象时要使用的给定构造函数，甚至是受保护的程序包。构造函数参数按名称映射到检索到的Document中的键值。
     * @Field 作用在成员变量，标记为文档的字段，并指定字段映射属性：
     * type：字段类型，取值是枚举：FieldType
     * index：是否索引，布尔类型，默认是true
     * store：是否存储，布尔类型，默认是false
     * analyzer：插入是使用分词器名称
     * searchAnalyzer :查询是使用分词器
     */
    @Id
    Long id;

    /**
     * 名称
     */
    @Field(type = FieldType.Text,searchAnalyzer = "ik_max_word",analyzer = "ik_max_word")
    String name;
    /**
     * 分类
     */
    @Field(type = FieldType.Keyword)
    String category;
    /**
     * 品牌
     */
    @Field(type = FieldType.Keyword)
    String brand;
    /**
     * 价格
     */
    @Field(type = FieldType.Double)
    Double price;
}
