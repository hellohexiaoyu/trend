package com.hexy.trend.controller;

import com.hexy.trend.entity.Product;
import com.hexy.trend.web.result.Result;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.IndexOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/es")
@RestController
public class EsController {

    @Autowired
    private ElasticsearchRestTemplate template;
    @PostMapping("/1")
    public Result test(){
//        template.indexOps(Product.class).delete();
////        boolean b = template.indexOps(Product.class).create();
//
//        Product p = new Product();
//        p.setId(1L);
//        p.setBrand("apple");
//        p.setCategory("phone");
//        p.setName("apple phone");
//        p.setPrice(8600.00);
//        Product save = template.save(p);


        NativeSearchQueryBuilder nb = new NativeSearchQueryBuilder();
        MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("name", "apple phone");
        NativeSearchQueryBuilder nativeSearchQueryBuilder = nb.withQuery(matchQuery);
        SearchHits<Product> search = template.search(nativeSearchQueryBuilder.build(), Product.class);
        search.getSearchHits().forEach(System.out::println);

        return Result.success(search);
    }
}
