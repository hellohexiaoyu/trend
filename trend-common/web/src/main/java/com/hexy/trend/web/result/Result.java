package com.hexy.trend.web.result;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


@Data
public class Result<T> implements Serializable {

    private Integer code;

    private String msg;

    private T data;
    public Result() {

    }
    public Result(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static Result success() {
        return new Result(ResultCode.SUCCESS.code(), ResultCode.SUCCESS.msg(), null);
    }

    public static Result success(Object data) {
        return new Result(ResultCode.SUCCESS.code(), ResultCode.SUCCESS.msg(), data);
    }

    public static Result failure(ResultCode resultCode) {
        return new Result(resultCode.code(), resultCode.msg(), null);
    }

    public static Result failure(ResultCode resultCode, Object data) {
        return new Result(resultCode.code(), resultCode.msg(), data);
    }

    public static Result failure(Integer code, String msg) {
        return new Result(code, msg, null);
    }

    public static Result failure(Integer code, String msg, Object data) {
        return new Result(code, msg, data);
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("code", this.code);
        map.put("msg", this.msg);
        map.put("data", this.data);
        return map;
    }
}
