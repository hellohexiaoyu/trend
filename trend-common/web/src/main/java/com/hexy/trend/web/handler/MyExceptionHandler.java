package com.hexy.trend.web.handler;


import com.hexy.trend.web.exception.BusinessException;
import com.hexy.trend.web.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Slf4j
@ResponseBody
@ControllerAdvice
public class MyExceptionHandler {

    // 普通未知异常
    @ExceptionHandler(value = Exception.class)
    public Result commonExceptionHandler(Exception e) {
        e.printStackTrace();
        log.warn("### CommonException : {}", e.getMessage());
        return Result.failure(20000, e.getMessage());
    }

    // 参数验证异常
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result validationExceptionHandler(MethodArgumentNotValidException e) {
        log.warn("### ValidationException : {}", e.getMessage());
        BindingResult result = e.getBindingResult();
        if (result.hasErrors()) {
            StringBuilder builder = new StringBuilder();
            List<FieldError> errors = result.getFieldErrors();
            for (FieldError error : errors) {
                builder.append(error.getField() + ":" + error.getDefaultMessage() + ";");
            }
            return Result.failure(10000, builder.toString());
        }
        return Result.failure(10000, e.getMessage());
    }

    // 业务处理异常
    @ExceptionHandler(value = BusinessException.class)
    public Result businessExceptionHandler(BusinessException e) {
        log.warn("### BusinessException : {}", e.getMessage());
        return Result.failure(e.getCode(), e.getMessage());
    }
}
