package com.hexy.trend.web.converter;


import com.alibaba.fastjson.JSON;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Objects;

@Component
public class ConverterWorker {
    /**
     * 15位身份证号
     */
    private static final Integer FIFTEEN_ID_CARD=15;
    /**
     * 18位身份证号
     */
    private static final Integer EIGHTEEN_ID_CARD=18;
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * 对象转json字符串
     * @param obj
     * @return
     */
    @Named("objectToJsonString")
    public String objectToJsonString(Object obj) {
        if (Objects.isNull(obj)) {
            return null;
        }
        return JSON.toJSONString(obj);
    }

    public static void main(String[] args) {

    }
}
