package com.hexy.trend.appsecurity.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class MyLoginAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws AuthenticationException {
        log.info("登录过滤，处理json!");
        HttpServletRequest request= (HttpServletRequest) servletRequest;
        HttpServletResponse response= (HttpServletResponse) servletResponse;
        if (!request.getMethod().equals("POST")) {//必须post登录
            throw new AuthenticationServiceException(
                    "Authentication method not supported: " + request.getMethod());
        }
        //如果是application/json类型，做如下处理
        if(request.getContentType() != null && (request.getContentType().equals("application/json;charset=UTF-8")||request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE))){
            //以json形式处理数据
            String username = null;
            String password = null;
//            String loginType = null;

            try {
                //将请求中的数据转为map
                @SuppressWarnings("unchecked")
                Map<String,String> map = new ObjectMapper().readValue(request.getInputStream(), Map.class);
                username = map.get("username");
                password = map.get("password");
//                loginType = map.get("loginType");
            } catch (IOException e) {
                e.printStackTrace();
            }
            username = username == null ? "":username.trim();
            password = password == null ? "":password.trim();
//            loginType = loginType == null ? "":loginType.trim();

//            MyAuthenticationToken authRequest = new MyAuthenticationToken(username, password,loginType);
            MyAuthenticationToken authRequest = new MyAuthenticationToken(username, password);
            setDetails(servletRequest, authRequest);

            return this.getAuthenticationManager().authenticate(authRequest);
        }
        //否则使用官方默认处理方式
        return super.attemptAuthentication(request, response);
    }
}
