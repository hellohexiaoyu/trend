package com.hexy.trend.appsecurity.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Slf4j
@Component
public class JwtTokenUtils {

    public static String HEADER = "Authorization";

    public static String PREFIX = "Bearer_";

    public static String SECRET = "123456";

    public static Long EXPIRE = 60 * 60 * 1000L;

    @Value("${jwt.header}")
    public void setHEADER(String HEADER) {
        JwtTokenUtils.HEADER = HEADER;
    }

    @Value("${jwt.prefix}")
    public void setPREFIX(String PREFIX) {
        JwtTokenUtils.PREFIX = PREFIX;
    }

    @Value("${jwt.secret}")
    public void setSECRET(String SECRET) {
        JwtTokenUtils.SECRET = SECRET;
    }

    @Value("${jwt.expire}")
    public void setEXPIRE(Long EXPIRE) {
        JwtTokenUtils.EXPIRE = EXPIRE;
    }


    /**
     * 检查token是否过期
     * @param token
     * @return
     */
    public static boolean isTokenExpired(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();

        Date expirationDate = claims.getExpiration();
        Date currentDate = new Date();

        return expirationDate.before(currentDate);
    }

    // 生成JWT(claims是携带的信息)
    public static String generate(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + EXPIRE))
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }


    // 解析JWT生成Claims
    public static Claims parse(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();

        //token过期验证
//        Date expirationDate = claims.getExpiration();
//        Date currentDate = new Date();
//        if(!expirationDate.before(currentDate)){
//            throw new BusinessException("Token已失效");
//        }

        return claims;
    }


    // 从request中获取Claims
    public static Claims getClaims(HttpServletRequest request) {
        String header = request.getHeader(HEADER);
        if (header != null && header.startsWith(PREFIX)) {
            // 截取token
            String token = header.substring(JwtTokenUtils.PREFIX.length());
            // 解析token
            return JwtTokenUtils.parse(token);
        }
        return null;
    }


    // 从request中获取Claims的字段
    public static Object getField(HttpServletRequest request, String key) {
        Claims claims = getClaims(request);
        if (claims != null && claims.containsKey(key)) {
            return claims.get(key);
        } else {
            throw new RuntimeException("Token字段异常:" + key);
        }
    }


    // 从request中获取Claims的字段
    public static Object getFieldMayNull(HttpServletRequest request, String key) {
        Claims claims = getClaims(request);
        if (claims != null) {
            return claims.get(key);
        }
        return null;
    }



    public static void main(String[] args) throws InterruptedException {
        HashMap<String, Object> map = new HashMap<>();
        map.put("customer", "aaa");
        map.put("data", "xxx");

        String jwt = JwtTokenUtils.generate(map);
        System.out.println(jwt);

        Claims claims = JwtTokenUtils.parse(jwt);
        System.out.println(claims);
    }
}
