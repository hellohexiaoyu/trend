package com.hexy.trend.appsecurity.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hexy.trend.web.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException e)
            throws IOException, ServletException {

        Integer code = 10000;
        String msg = "";

        if (e instanceof UsernameNotFoundException) {
            log.info("用户名不存在 {}", e.getMessage());
            code = 10001;
            msg = "用户名不存在";
        }
        else if (e instanceof BadCredentialsException) {
            log.info("用户密码错误 {}", e.getMessage());
            code = 10002;
            msg = "用户密码错误";
        }
        else if (e instanceof LockedException) {
            log.info("用户已被锁定 {}", e.getMessage());
            code = 10003;
            msg = "用户已被锁定";
        }else if(e instanceof DisabledException){
            log.info("用户已被禁用 {}", e.getMessage());
            code = 10004;
            msg = "用户已被禁用";
        }

        Result result = Result.failure(code, msg);

        // 设置header
        response.setStatus(HttpStatus.OK.value());
        response.setContentType("application/json;charset=UTF-8");

        // 设置body
        response.getWriter().write(objectMapper.writeValueAsString(result));
        response.getWriter().flush();
        response.getWriter().close();
    }
}
