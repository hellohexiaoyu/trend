package com.hexy.trend.appsecurity.filter;


import com.hexy.trend.appsecurity.utils.JwtTokenUtils;
import com.hexy.trend.web.exception.BusinessException;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
public class MyJwtAuthenticationFilter extends BasicAuthenticationFilter {

    public MyJwtAuthenticationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain)
            throws ServletException, IOException {
        try {
            // 获取header
            String header = request.getHeader(JwtTokenUtils.HEADER);

            // 处理header
            if (header != null && header.startsWith(JwtTokenUtils.PREFIX)) {

                // 截取token
                String token = header.substring(JwtTokenUtils.PREFIX.length());

                // 解析token
                Claims claims = null;
                try {
                    claims = JwtTokenUtils.parse(token);
                }catch (Exception e){
                    throw new BusinessException(999999,"token失效");
                }


                // 解析username
                String username = (String) claims.get("username");

                // 生成authentication
                if (username != null) {

                    // 后续不需要做权限认证，可以直接生成authenticationToken
                    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(null, null, null);
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);

                    //                // 后续需要做权限认证，则需要查询当前用户的访问权限，生成authenticationToken
                    //                // 获取context
                    //                ServletContext servletContext = request.getServletContext();
                    //                ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
                    //
                    //                // 获取service
                    //                UserDetailsService detailsService = applicationContext.getBean(UserDetailsService.class);
                    //
                    //                // 查询信息
                    //                UserDetails userDetails = detailsService.loadUserByUsername(username);
                    //
                    //                // 设置Authentication
                    //                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    //                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    //                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }
            }

            // 没有header或非法token
            else {
            }

            // 继续后续过滤器
            chain.doFilter(request, response);

        } catch (BusinessException e) {
            // 捕获 BusinessException 异常，并将提示信息添加到响应中
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType("text/plain;charset=UTF-8");
            response.getWriter().write(e.getMessage());
        }
    }
}
