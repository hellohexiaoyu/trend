package com.hexy.trend.appsecurity.base;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;


public interface JwtUserDetails extends UserDetails {

    /**
     * 将UserDetail转换成Map，存入jwt中需要用到map类型结构
     * 注意：必须包含username字段
     *
     * @return
     */
    Map<String, Object> toClaimsMap();

}
