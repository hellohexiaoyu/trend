package com.hexy.trend.appsecurity.filter;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class MyAuthenticationToken extends UsernamePasswordAuthenticationToken {
    /**
     *
     */
    private static final long serialVersionUID = 2112284640094900016L;

    private String loginType;

    public MyAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
        super.setAuthenticated(false);
    }
    public MyAuthenticationToken(Object principal, Object credentials, String loginType) {
        super(principal, credentials);
        this.loginType = loginType;
        super.setAuthenticated(false);
    }
    public MyAuthenticationToken(Object principal, Object credentials, String loginType,
                                 Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.loginType = loginType;
        super.setAuthenticated(true);
    }

    public String getLoginType() {
        return this.loginType;
    }
}