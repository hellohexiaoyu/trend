package com.hexy.trend.appsecurity.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hexy.trend.appsecurity.base.JwtUserDetails;
import com.hexy.trend.appsecurity.utils.JwtTokenUtils;
import com.hexy.trend.web.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


@Slf4j
@Component
public class MyAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication)
            throws IOException {

        // 获取信息
        JwtUserDetails jwtUserDetails = (JwtUserDetails) authentication.getPrincipal();
        Map<String, Object> claims = jwtUserDetails.toClaimsMap();

        // 生成token
        String jwt = JwtTokenUtils.generate(claims);
        String token = JwtTokenUtils.PREFIX + jwt;

        // 生成result
        Result result = Result.success(jwtUserDetails);

        // 设置header
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        response.setHeader(JwtTokenUtils.HEADER, token);
        response.setHeader("auth",claims.get("type").toString());
        // 设置body
        response.getWriter().write(objectMapper.writeValueAsString(result));
        response.getWriter().flush();
        response.getWriter().close();
    }
}
