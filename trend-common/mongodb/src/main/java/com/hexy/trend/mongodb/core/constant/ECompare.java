package com.hexy.trend.mongodb.core.constant;

/**
 * 比较类型
 *
 * @author hexy
 * @date 2023/6/13
 */
public enum ECompare {

    EQ,
    NE,
    LE,
    LT,
    GE,
    GT,
    BW,
    IN,
    NIN

}
