package com.hexy.trend.mongodb.core.constant;

/**
 * 排序类型
 *
 * @author hexy
 * @date 2023/6/13
 */
public enum ESortType {

    ASC,
    DESC

}
