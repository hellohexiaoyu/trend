package com.hexy.trend.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.*;
import com.hexy.trend.enums.DepartmentTypeWeekEnum;
import com.hexy.trend.enums.DepartmentTypeWeekEnum;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.*;
import com.hexy.trend.service.impl.InsightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ScheduledWeekTask {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TAmazonCaWeekService tAmazonCaService;
    @Autowired
    private TAmazonUsaWeekService tAmazonUsaService;
    @Autowired
    private TAmazonDeWeekService tAmazonDeService;
    @Autowired
    private TAmazonEsWeekService tAmazonEsService;
    @Autowired
    private TAmazonFrWeekService tAmazonFrService;
    @Autowired
    private TAmazonItWeekService tAmazonItService;
    @Autowired
    private TAmazonJpWeekService tAmazonJpService;
    @Autowired
    private TAmazonMxWeekService tAmazonMxService;
    @Autowired
    private TAmazonUkWeekService tAmazonUkService;

    @Autowired
    private InsightService insightService;
    /**
     * 会诊当天，发起方未手动签到状态更新为未签到
     */
//    @Scheduled(cron = "0 0 1 * * ?")//每天凌晨1点执行
//    @Scheduled(cron = "*/40 * * * * ?")
//    @Scheduled(cron = "0 30 23 * * ?")
//    @Scheduled(cron = "0 */30 * * * ?")
    @Scheduled(cron = "0 0 2 * * ?")//凌晨两点
    public void executeOutpatientQueueTask() throws ParseException {

        log.info("==========周数据批量任务开始============");
        long start = System.currentTimeMillis();
        // 获取当前时间
        Calendar calendar = Calendar.getInstance();
        Date day = calendar.getTime();
        // 往前推一年（修改为180天）
//        calendar.add(Calendar.YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, -180);

        // 将Calendar对象转换为Date对象
        Date oneYearAgoDate = calendar.getTime();

        log.info("==================>>>>>>缓存趋势开始：{}",start);
        //数据清洗
        log.info("==================>>>>>>开始清洗数据");
        //清洗重复数据
        insightService.clearDataWeek();

        log.info("==================>>>>>>结束清洗数据");



//        log.info("==================>>>>>>开始缓存数据");
//        //缓存数据
//        InsightRO.GetInsightDepartmentRO ro = new InsightRO.GetInsightDepartmentRO();
//        ro.setSDate(oneYearAgoDate);
//        ro.setEDate(day);
//
//        int pageSize = 100000;       // 每页大小
//        Integer items = 0;          //总条数
//        int num = 0;                //总页数
//        //CA
//        items = tAmazonCaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_ca.getDesc() + ":");
//
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonCaWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonCaWeek> caData = tAmazonCaService.getRankingListTask(page, ro);
//                caStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//
//
//        //USA
//        items = tAmazonUsaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_com.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonUsaWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonUsaWeek> caData = tAmazonUsaService.getRankingListTask(page, ro);
//                usaStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //De
//        items = tAmazonDeService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_co_de.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonDeWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonDeWeek> caData = tAmazonDeService.getRankingListTask(page, ro);
//                deStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //ES
//        items = tAmazonEsService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_es.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonEsWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonEsWeek> caData = tAmazonEsService.getRankingListTask(page, ro);
//                esStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Fr
//        items = tAmazonFrService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_fr.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonFrWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonFrWeek> caData = tAmazonFrService.getRankingListTask(page, ro);
//                frStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //It
//        items = tAmazonItService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_it.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonItWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonItWeek> caData = tAmazonItService.getRankingListTask(page, ro);
//                itStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Jp
//        items = tAmazonJpService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_co_jp.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonJpWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonJpWeek> caData = tAmazonJpService.getRankingListTask(page, ro);
//                jpStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Mx
//        items = tAmazonMxService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_com_mx.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonMxWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonMxWeek> caData = tAmazonMxService.getRankingListTask(page, ro);
//                mxStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //Uk
//        items = tAmazonUkService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeWeekEnum.amazon_co_uk.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonUkWeek> page = new Page<>(i, pageSize);
//                Page<TAmazonUkWeek> caData = tAmazonUkService.getRankingListTask(page, ro);
//                ukStoreDataInRedis(caData.getRecords());
//            }
//        }
//        log.info("==================>>>>>>结束缓存数据");
        long end = System.currentTimeMillis();
        log.info("==================>>>>>>缓存趋势结束：{},用时：{}",end,end - start);
    }

    public void clearAllCache(String key) {
        // 获取匹配指定前缀的所有键
        Set<String> keysToDelete = redisUtil.keys(key + "*");
        // 删除匹配的所有键
        if (!keysToDelete.isEmpty()) {
            redisUtil.delBatch(keysToDelete);
        }
    }

    public void caStoreDataInRedis(List<TAmazonCaWeek> allData) {
        Map<String, List<TAmazonCaWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonCaWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonCaWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonCaWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonCaWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_ca.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonCaWeek> tAmazonCas = JSON.parseArray(s, TAmazonCaWeek.class);
                tAmazonCas.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonCas, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void usaStoreDataInRedis(List<TAmazonUsaWeek> allData) {

        Map<String, List<TAmazonUsaWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUsaWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUsaWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUsaWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUsaWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_com.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUsaWeek> tAmazonUsa = JSON.parseArray(s, TAmazonUsaWeek.class);
                tAmazonUsa.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUsa, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void deStoreDataInRedis(List<TAmazonDeWeek> allData) {

        Map<String, List<TAmazonDeWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonDeWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonDeWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonDeWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonDeWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_co_de.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonDeWeek> tAmazonDe = JSON.parseArray(s, TAmazonDeWeek.class);
                tAmazonDe.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonDe, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void esStoreDataInRedis(List<TAmazonEsWeek> allData) {

        Map<String, List<TAmazonEsWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonEsWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonEsWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonEsWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonEsWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_es.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonEsWeek> tAmazonEs = JSON.parseArray(s, TAmazonEsWeek.class);
                tAmazonEs.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonEs, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void frStoreDataInRedis(List<TAmazonFrWeek> allData) {

        Map<String, List<TAmazonFrWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonFrWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonFrWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonFrWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonFrWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_fr.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonFrWeek> tAmazonFr = JSON.parseArray(s, TAmazonFrWeek.class);
                tAmazonFr.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonFr, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void itStoreDataInRedis(List<TAmazonItWeek> allData) {

        Map<String, List<TAmazonItWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonItWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonItWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonItWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonItWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_it.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonItWeek> tAmazonIt = JSON.parseArray(s, TAmazonItWeek.class);
                tAmazonIt.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonIt, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void jpStoreDataInRedis(List<TAmazonJpWeek> allData) {

        Map<String, List<TAmazonJpWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonJpWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonJpWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonJpWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonJpWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_co_jp.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonJpWeek> tAmazonJp = JSON.parseArray(s, TAmazonJpWeek.class);
                tAmazonJp.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonJp, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }


    public void mxStoreDataInRedis(List<TAmazonMxWeek> allData) {

        Map<String, List<TAmazonMxWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonMxWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonMxWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonMxWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonMxWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_com_mx.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonMxWeek> tAmazonMx = JSON.parseArray(s, TAmazonMxWeek.class);
                tAmazonMx.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonMx, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void ukStoreDataInRedis(List<TAmazonUkWeek> allData) {

        Map<String, List<TAmazonUkWeek>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUkWeek::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUkWeek>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUkWeek> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUkWeek::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeWeekEnum.amazon_co_uk.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUkWeek> tAmazonUk = JSON.parseArray(s, TAmazonUkWeek.class);
                tAmazonUk.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUk, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }
}
