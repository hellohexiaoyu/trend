package com.hexy.trend.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.*;
import com.hexy.trend.enums.DepartmentTypeMonthEnum;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.*;
import com.hexy.trend.service.impl.InsightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ScheduledMonthTask {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TAmazonCaMonthService tAmazonCaService;
    @Autowired
    private TAmazonUsaMonthService tAmazonUsaService;
    @Autowired
    private TAmazonDeMonthService tAmazonDeService;
    @Autowired
    private TAmazonEsMonthService tAmazonEsService;
    @Autowired
    private TAmazonFrMonthService tAmazonFrService;
    @Autowired
    private TAmazonItMonthService tAmazonItService;
    @Autowired
    private TAmazonJpMonthService tAmazonJpService;
    @Autowired
    private TAmazonMxMonthService tAmazonMxService;
    @Autowired
    private TAmazonUkMonthService tAmazonUkService;

    @Autowired
    private InsightService insightService;
    /**
     * 会诊当天，发起方未手动签到状态更新为未签到
     */
//    @Scheduled(cron = "0 0 1 * * ?")//每天凌晨1点执行
//    @Scheduled(cron = "*/40 * * * * ?")
//    @Scheduled(cron = "0 30 23 * * ?")
//    @Scheduled(cron = "0 */30 * * * ?")
    @Scheduled(cron = "0 30 2 * * ?")//凌晨三点
    public void executeOutpatientQueueTask() throws ParseException {
        log.info("==========月数据批量任务开始============");
        long start = System.currentTimeMillis();
        // 获取当前时间
        Calendar calendar = Calendar.getInstance();
        Date day = calendar.getTime();
        // 往前推一年（修改为180天）
//        calendar.add(Calendar.YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, -180);

        // 将Calendar对象转换为Date对象
        Date oneYearAgoDate = calendar.getTime();

        log.info("==================>>>>>>缓存趋势开始：{}",start);
        //数据清洗
        log.info("==================>>>>>>开始清洗数据");
        //清洗重复数据
        insightService.clearDataMonth();

        log.info("==================>>>>>>结束清洗数据");



//        log.info("==================>>>>>>开始缓存数据");
//        //缓存数据
//        InsightRO.GetInsightDepartmentRO ro = new InsightRO.GetInsightDepartmentRO();
//        ro.setSDate(oneYearAgoDate);
//        ro.setEDate(day);
//
//        int pageSize = 100000;       // 每页大小
//        Integer items = 0;          //总条数
//        int num = 0;                //总页数
//        //CA
//        items = tAmazonCaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_ca.getDesc() + ":");
//
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonCaMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonCaMonth> caData = tAmazonCaService.getRankingListTask(page, ro);
//                caStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//
//
//        //USA
//        items = tAmazonUsaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_com.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonUsaMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonUsaMonth> caData = tAmazonUsaService.getRankingListTask(page, ro);
//                usaStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //De
//        items = tAmazonDeService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_co_de.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonDeMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonDeMonth> caData = tAmazonDeService.getRankingListTask(page, ro);
//                deStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //ES
//        items = tAmazonEsService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_es.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonEsMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonEsMonth> caData = tAmazonEsService.getRankingListTask(page, ro);
//                esStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Fr
//        items = tAmazonFrService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_fr.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonFrMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonFrMonth> caData = tAmazonFrService.getRankingListTask(page, ro);
//                frStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //It
//        items = tAmazonItService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_it.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonItMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonItMonth> caData = tAmazonItService.getRankingListTask(page, ro);
//                itStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Jp
//        items = tAmazonJpService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_co_jp.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonJpMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonJpMonth> caData = tAmazonJpService.getRankingListTask(page, ro);
//                jpStoreDataInRedis(caData.getRecords());
//            }
//        }
//
//        //Mx
//        items = tAmazonMxService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_com_mx.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonMxMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonMxMonth> caData = tAmazonMxService.getRankingListTask(page, ro);
//                mxStoreDataInRedis(caData.getRecords());
//            }
//        }
//        //Uk
//        items = tAmazonUkService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeMonthEnum.amazon_co_uk.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            for (int i = 1; i <= num; i++) {
//                Page<TAmazonUkMonth> page = new Page<>(i, pageSize);
//                Page<TAmazonUkMonth> caData = tAmazonUkService.getRankingListTask(page, ro);
//                ukStoreDataInRedis(caData.getRecords());
//            }
//        }
//        log.info("==================>>>>>>结束缓存数据");
        long end = System.currentTimeMillis();
        log.info("==================>>>>>>缓存趋势结束：{},用时：{}",end,end - start);
    }

    public void clearAllCache(String key) {
        // 获取匹配指定前缀的所有键
        Set<String> keysToDelete = redisUtil.keys(key + "*");
        // 删除匹配的所有键
        if (!keysToDelete.isEmpty()) {
            redisUtil.delBatch(keysToDelete);
        }
    }

    public void caStoreDataInRedis(List<TAmazonCaMonth> allData) {
        Map<String, List<TAmazonCaMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonCaMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonCaMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonCaMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonCaMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_ca.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonCaMonth> tAmazonCas = JSON.parseArray(s, TAmazonCaMonth.class);
                tAmazonCas.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonCas, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void usaStoreDataInRedis(List<TAmazonUsaMonth> allData) {

        Map<String, List<TAmazonUsaMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUsaMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUsaMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUsaMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUsaMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_com.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUsaMonth> tAmazonUsa = JSON.parseArray(s, TAmazonUsaMonth.class);
                tAmazonUsa.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUsa, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void deStoreDataInRedis(List<TAmazonDeMonth> allData) {

        Map<String, List<TAmazonDeMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonDeMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonDeMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonDeMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonDeMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_co_de.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonDeMonth> tAmazonDe = JSON.parseArray(s, TAmazonDeMonth.class);
                tAmazonDe.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonDe, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void esStoreDataInRedis(List<TAmazonEsMonth> allData) {

        Map<String, List<TAmazonEsMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonEsMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonEsMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonEsMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonEsMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_es.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonEsMonth> tAmazonEs = JSON.parseArray(s, TAmazonEsMonth.class);
                tAmazonEs.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonEs, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void frStoreDataInRedis(List<TAmazonFrMonth> allData) {

        Map<String, List<TAmazonFrMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonFrMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonFrMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonFrMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonFrMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_fr.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonFrMonth> tAmazonFr = JSON.parseArray(s, TAmazonFrMonth.class);
                tAmazonFr.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonFr, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void itStoreDataInRedis(List<TAmazonItMonth> allData) {

        Map<String, List<TAmazonItMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonItMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonItMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonItMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonItMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_it.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonItMonth> tAmazonIt = JSON.parseArray(s, TAmazonItMonth.class);
                tAmazonIt.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonIt, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void jpStoreDataInRedis(List<TAmazonJpMonth> allData) {

        Map<String, List<TAmazonJpMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonJpMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonJpMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonJpMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonJpMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_co_jp.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonJpMonth> tAmazonJp = JSON.parseArray(s, TAmazonJpMonth.class);
                tAmazonJp.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonJp, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }


    public void mxStoreDataInRedis(List<TAmazonMxMonth> allData) {

        Map<String, List<TAmazonMxMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonMxMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonMxMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonMxMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonMxMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_com_mx.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonMxMonth> tAmazonMx = JSON.parseArray(s, TAmazonMxMonth.class);
                tAmazonMx.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonMx, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void ukStoreDataInRedis(List<TAmazonUkMonth> allData) {

        Map<String, List<TAmazonUkMonth>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUkMonth::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUkMonth>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUkMonth> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUkMonth::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeMonthEnum.amazon_co_uk.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUkMonth> tAmazonUk = JSON.parseArray(s, TAmazonUkMonth.class);
                tAmazonUk.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUk, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }
}
