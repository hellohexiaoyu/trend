package com.hexy.trend.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.*;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.*;
import com.hexy.trend.service.impl.InsightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ScheduledTask {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private TAmazonCaService tAmazonCaService;
    @Autowired
    private TAmazonUsaService tAmazonUsaService;
    @Autowired
    private TAmazonDeService tAmazonDeService;
    @Autowired
    private TAmazonEsService tAmazonEsService;
    @Autowired
    private TAmazonFrService tAmazonFrService;
    @Autowired
    private TAmazonItService tAmazonItService;
    @Autowired
    private TAmazonJpService tAmazonJpService;
    @Autowired
    private TAmazonMxService tAmazonMxService;
    @Autowired
    private TAmazonUkService tAmazonUkService;

    @Autowired
    private InsightService insightService;

    private static final int THREAD_POOL_SIZE = 2; // 你想要的线程池大小
    private static final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    /**
     * 会诊当天，发起方未手动签到状态更新为未签到
     */
//    @Scheduled(cron = "0 0 1 * * ?")//每天凌晨1点执行
//    @Scheduled(cron = "*/40 * * * * ?")
//    @Scheduled(cron = "0 30 23 * * ?")
//    @Scheduled(cron = "0 */30 * * * ?")
    @Scheduled(cron = "0 0 3 * * ?")//凌晨两点
    public void executeOutpatientQueueTask() throws ParseException {
        log.info("==========天数据批量任务开始============");
        long start = System.currentTimeMillis();
        // 获取当前时间
        Calendar calendar = Calendar.getInstance();
        Date day = calendar.getTime();
        // 往前推一年（修改为180天）
//        calendar.add(Calendar.YEAR, -1);
        calendar.add(Calendar.DAY_OF_YEAR, -180);

        // 将Calendar对象转换为Date对象
        Date oneYearAgoDate = calendar.getTime();

        log.info("==================>>>>>>缓存趋势开始：{}",start);
        //数据清洗
        log.info("==================>>>>>>开始清洗数据");
        //清洗重复数据
        insightService.clearData();
        //删除过期数据（180天）
        insightService.delData(oneYearAgoDate);
        log.info("==================>>>>>>结束清洗数据");



//        log.info("==================>>>>>>开始缓存数据");
//        //缓存数据
//        InsightRO.GetInsightDepartmentRO ro = new InsightRO.GetInsightDepartmentRO();
//        ro.setSDate(oneYearAgoDate);
//        ro.setEDate(day);
//
//        int pageSize = 100000;       // 每页大小
//        Integer items = 0;          //总条数
//        int num = 0;                //总页数
//        //CA
//        items = tAmazonCaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_ca.getDesc() + ":");
//
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processCaDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//
//
//
//        //USA
//        items = tAmazonUsaService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_com.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processUsaDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//        //De
//        items = tAmazonDeService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_co_de.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processDeDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//
//        //ES
//        items = tAmazonEsService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_es.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processEsDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//
//        //Fr
//        items = tAmazonFrService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_fr.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processFrDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//        //It
//        items = tAmazonItService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_it.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processItDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//
//        //Jp
//        items = tAmazonJpService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_co_jp.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processJpDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//
//        //Mx
//        items = tAmazonMxService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_com_mx.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processMxDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//        //Uk
//        items = tAmazonUkService.getCount(ro);
//        if (items > 0) {
//            clearAllCache("trend:" + DepartmentTypeEnum.amazon_co_uk.getDesc() + ":");
//            num = (int) Math.ceil((double) items / pageSize);
//            int finalNum = num;
//            CompletableFuture<Void> caFuture = CompletableFuture.runAsync(() -> processUkDataAsync(finalNum, pageSize, ro), executorService);
//            waitAsync(caFuture);
//        }
//        executorService.shutdown();
//        log.info("==================>>>>>>结束缓存数据");
//        long end = System.currentTimeMillis();
//        log.info("==================>>>>>>缓存趋势结束：{},用时：{}",end,end - start);
    }

    public void clearAllCache(String key) {
        // 获取匹配指定前缀的所有键
        Set<String> keysToDelete = redisUtil.keys(key + "*");
        // 删除匹配的所有键
        if (!keysToDelete.isEmpty()) {
            redisUtil.delBatch(keysToDelete);
        }
    }


    public void caStoreDataInRedis(List<TAmazonCa> allData) {
        Map<String, List<TAmazonCa>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonCa::getSearchTerm));

        for (Map.Entry<String, List<TAmazonCa>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonCa> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonCa::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_ca.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonCa> tAmazonCas = JSON.parseArray(s, TAmazonCa.class);
                tAmazonCas.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonCas, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void usaStoreDataInRedis(List<TAmazonUsa> allData) {

        Map<String, List<TAmazonUsa>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUsa::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUsa>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUsa> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUsa::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_com.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUsa> tAmazonUsa = JSON.parseArray(s, TAmazonUsa.class);
                tAmazonUsa.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUsa, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void deStoreDataInRedis(List<TAmazonDe> allData) {

        Map<String, List<TAmazonDe>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonDe::getSearchTerm));

        for (Map.Entry<String, List<TAmazonDe>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonDe> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonDe::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_co_de.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonDe> tAmazonDe = JSON.parseArray(s, TAmazonDe.class);
                tAmazonDe.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonDe, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void esStoreDataInRedis(List<TAmazonEs> allData) {

        Map<String, List<TAmazonEs>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonEs::getSearchTerm));

        for (Map.Entry<String, List<TAmazonEs>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonEs> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonEs::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_es.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonEs> tAmazonEs = JSON.parseArray(s, TAmazonEs.class);
                tAmazonEs.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonEs, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void frStoreDataInRedis(List<TAmazonFr> allData) {

        Map<String, List<TAmazonFr>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonFr::getSearchTerm));

        for (Map.Entry<String, List<TAmazonFr>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonFr> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonFr::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_fr.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonFr> tAmazonFr = JSON.parseArray(s, TAmazonFr.class);
                tAmazonFr.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonFr, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void itStoreDataInRedis(List<TAmazonIt> allData) {

        Map<String, List<TAmazonIt>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonIt::getSearchTerm));

        for (Map.Entry<String, List<TAmazonIt>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonIt> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonIt::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_it.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonIt> tAmazonIt = JSON.parseArray(s, TAmazonIt.class);
                tAmazonIt.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonIt, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void jpStoreDataInRedis(List<TAmazonJp> allData) {

        Map<String, List<TAmazonJp>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonJp::getSearchTerm));

        for (Map.Entry<String, List<TAmazonJp>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonJp> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonJp::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_co_jp.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonJp> tAmazonJp = JSON.parseArray(s, TAmazonJp.class);
                tAmazonJp.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonJp, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }


    public void mxStoreDataInRedis(List<TAmazonMx> allData) {

        Map<String, List<TAmazonMx>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonMx::getSearchTerm));

        for (Map.Entry<String, List<TAmazonMx>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonMx> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonMx::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_com_mx.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonMx> tAmazonMx = JSON.parseArray(s, TAmazonMx.class);
                tAmazonMx.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonMx, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }

    public void ukStoreDataInRedis(List<TAmazonUk> allData) {

        Map<String, List<TAmazonUk>> groupedData = allData.stream()
                .collect(Collectors.groupingBy(TAmazonUk::getSearchTerm));

        for (Map.Entry<String, List<TAmazonUk>> entry : groupedData.entrySet()) {
            String searchTerm = entry.getKey();
            List<TAmazonUk> dataList = entry.getValue();
            // 按时间顺序排序
            dataList.sort(Comparator.comparing(TAmazonUk::getDate));
            // 存储到Redis，假设使用列表
            String redisKey = "trend:" + DepartmentTypeEnum.amazon_co_uk.getDesc() + ":" + searchTerm;
            String s = redisUtil.get(redisKey);
            if (null != s) {
                List<TAmazonUk> tAmazonUk = JSON.parseArray(s, TAmazonUk.class);
                tAmazonUk.addAll(dataList);
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(tAmazonUk, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            } else {
                redisUtil.set(redisKey, JSON.toJSONStringWithDateFormat(dataList, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
            }
        }
    }
    private void waitAsync(CompletableFuture<Void> future) {
        try {
            future.get(); // 等待任务完成
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace(); // 处理异常
        }
    }
    private void processCaDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonCa> page = new Page<>(i, pageSize);
            Page<TAmazonCa> caData = tAmazonCaService.getRankingListTask(page, ro);
            caStoreDataInRedis(caData.getRecords());
        }
    }

    private void processUsaDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonUsa> page = new Page<>(i, pageSize);
            Page<TAmazonUsa> usaData = tAmazonUsaService.getRankingListTask(page, ro);
            usaStoreDataInRedis(usaData.getRecords());
        }
    }
    private void processDeDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonDe> page = new Page<>(i, pageSize);
            Page<TAmazonDe> deData = tAmazonDeService.getRankingListTask(page, ro);
            deStoreDataInRedis(deData.getRecords());
        }
    }

    private void processEsDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonEs> page = new Page<>(i, pageSize);
            Page<TAmazonEs> caData = tAmazonEsService.getRankingListTask(page, ro);
            esStoreDataInRedis(caData.getRecords());
        }
    }

    private void processFrDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonFr> page = new Page<>(i, pageSize);
            Page<TAmazonFr> caData = tAmazonFrService.getRankingListTask(page, ro);
            frStoreDataInRedis(caData.getRecords());
        }
    }

    private void processItDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonIt> page = new Page<>(i, pageSize);
            Page<TAmazonIt> caData = tAmazonItService.getRankingListTask(page, ro);
            itStoreDataInRedis(caData.getRecords());
        }
    }

    private void processJpDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonJp> page = new Page<>(i, pageSize);
            Page<TAmazonJp> caData = tAmazonJpService.getRankingListTask(page, ro);
            jpStoreDataInRedis(caData.getRecords());
        }
    }

    private void processMxDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonMx> page = new Page<>(i, pageSize);
            Page<TAmazonMx> caData = tAmazonMxService.getRankingListTask(page, ro);
            mxStoreDataInRedis(caData.getRecords());
        }
    }

    private void processUkDataAsync(int num, int pageSize, InsightRO.GetInsightDepartmentRO ro) {
        for (int i = 1; i <= num; i++) {
            Page<TAmazonUk> page = new Page<>(i, pageSize);
            Page<TAmazonUk> caData = tAmazonUkService.getRankingListTask(page, ro);
            ukStoreDataInRedis(caData.getRecords());
        }
    }
}
