package com.hexy.trend.job;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.*;
import com.hexy.trend.enums.AcountTypeEnum;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.*;
import com.hexy.trend.service.impl.InsightService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Component
public class VipExpireScheduledTask {

    @Autowired
    private TAmazonUserService service;

    @Scheduled(cron = "0 */10 * * * ?")//十分钟检测一次
    public void executeOutpatientQueueTask() throws ParseException {
        log.info("==========VIP用户过期检测开始============");
        long start = System.currentTimeMillis();
        List<TAmazonUser> userByType = service.getUserByType(AcountTypeEnum.VIP.getValue());
        // 获取当前时间
        Date currentDate = new Date();

        for(TAmazonUser users : userByType){
            if(currentDate.before(users.getStartTime()) || currentDate.after(users.getEndTime())){
                TAmazonUser setUser = new TAmazonUser();
                setUser.setId(users.getId());
                setUser.setType(AcountTypeEnum.GENERAL.getValue());
                setUser.setUpdatedAt(new Date());
                service.setUser(setUser);
            }
        }
        long end = System.currentTimeMillis();
        log.info("==========VIP用户过期检测结束============");
        log.info("==================>>>>>>VIP用户过期检测：{},用时：{}",end,end - start);
    }

}
