package com.hexy.trend.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.appsecurity.utils.JwtTokenUtils;
import com.hexy.trend.bean.aci.UserCI;
import com.hexy.trend.bean.aro.UserRO;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonUser;
import com.hexy.trend.enums.AcountTypeEnum;
import com.hexy.trend.mail.MailMessage;
import com.hexy.trend.mail.MailSender;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.TAmazonUserService;
import com.hexy.trend.util.TrendCommonUtils;
import com.hexy.trend.web.exception.BusinessException;
import com.hexy.trend.web.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
@Api(tags = "用户")
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private TAmazonUserService user;

    @Resource
    private MailSender mailSender;

    @Autowired
    private RedisUtil redisUtil;
    @ApiOperation(value = "获取个人信息", notes = "获取个人信息")
    @PostMapping("/getUser")
    public Result<TAmazonUser> getUser(HttpServletRequest request) {
        //用户ID
        Integer id = (Integer) JwtTokenUtils.getField(request, "id");
        TAmazonUser amazonUser = user.getUserById(id);
        return Result.success(amazonUser);
    }

    @ApiOperation(value = "修改密码", notes = "修改密码")
    @PostMapping("/setPw")
    public Result setPw(HttpServletRequest request, @RequestBody @Valid UserRO.SetPwRO ro) {
        //验证密码两次输入是否一致
        if(!ro.getPassword().equals(ro.getConfirmPassword())){
            throw new BusinessException("输入密码不一致");
        }
        //用户ID
        Integer id = (Integer) JwtTokenUtils.getField(request, "id");
        TAmazonUser amazonUser = new TAmazonUser();
        amazonUser.setId(id);
        amazonUser.setPassword(ro.getPassword());
        amazonUser.setUpdatedAt(new Date());
        Integer integer = user.setUser(amazonUser);
        return Result.success();
    }

    @ApiOperation(value = "获取账号列表", notes = "获取账号列表")
    @PostMapping("/getUserList")
    public Result<Page<TAmazonUser>> getUserList(HttpServletRequest request, @RequestBody @Valid UserRO.GetUserListRO ro) {
        //用户ID
        Integer type = (Integer) JwtTokenUtils.getField(request, "type");
        if(type != AcountTypeEnum.ADMIN.getValue()){
            throw new BusinessException("当前账号权限不足");
        }
        Page<TAmazonUser> page = new Page<>(ro.getPageNum(),ro.getPageSize());
        Page<TAmazonUser> userList = user.getUserList(page, ro.getUserName(), ro.getType(), ro.getStartTime(), ro.getEndTime());
        return Result.success(userList);
    }

    @ApiOperation(value = "vip设置", notes = "vip设置")
    @PostMapping("/setUserVip")
    public Result setUserVip(HttpServletRequest request, @RequestBody @Valid UserRO.SetUserVipRO ro) {
        //用户ID
        Integer type = (Integer) JwtTokenUtils.getField(request, "type");
        if(type != AcountTypeEnum.ADMIN.getValue()){
            throw new BusinessException("当前账号权限不足");
        }
        TAmazonUser amazonUser = new TAmazonUser();
        amazonUser.setId(ro.getId());
        amazonUser.setType(AcountTypeEnum.VIP.getValue());
        amazonUser.setStartTime(ro.getStartTime());
        amazonUser.setEndTime(ro.getEndTime());
        amazonUser.setUpdatedAt(new Date());
        Integer integer = user.setUser(amazonUser);
        return Result.success();
    }

}

