//package com.hexy.trend.controller;
//
//
//import com.hexy.trend.bean.aro.TAmazonCaRO;
//import com.hexy.trend.config.InfluxDBProperties;
//import com.hexy.trend.handler.mongo.TAmazonCaHandler;
//import com.hexy.trend.web.result.Result;
//import com.influxdb.query.FluxTable;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.Valid;
//import java.util.*;
//
///**
// * <p>
// *  前端控制器
// * </p>
// *
// * @author hexy
// * @since 2023-09-26
// */
//@Api(tags = "导入/查询")
//@Slf4j
//@RestController
//@RequestMapping("/test")
//public class TestController {
//
//    @Autowired
//    private TAmazonCaHandler tAmazonCaHandler;
//    @ApiOperation(value = "mongo测试接口2", notes = "测试接口2")
//    @PostMapping("/addRankingList")
//    public Result<Boolean> addRankingList(HttpServletRequest request, @RequestBody @Valid TAmazonCaRO ro){
////        ObjectId objectId = new ObjectId();
////        ro.setId(objectId.toString());
//        ro.setId(1);
//        Result<Boolean> save = tAmazonCaHandler.save(ro);
//
//        return Result.success(save);
//    }
//    @Autowired
//    private InfluxDBProperties influxDBProperties;
//    @ApiOperation(value = "时序性数据库测试接口3", notes = "测试接口3")
//    @PostMapping("/addRankingList1")
//    public Result<Boolean> addRankingList1(HttpServletRequest request, @RequestBody @Valid TAmazonCaRO ro){
//
//        Map<String,Object> param = new HashMap<>();
//        param.put("department","amazon.ca");
//        param.put("searchTerm","mp4");
//        param.put("date",new Date());
//        param.put("sort","1");
//        influxDBProperties.save("t_amazon_ca",param);
//
//        List<Map<String,Object>> listParam = new ArrayList<>();
//        listParam.add(param);
//        listParam.add(param);
//        listParam.add(param);
//        listParam.add(param);
//
//        influxDBProperties.saveBatch("t_amazon_ca",listParam);
//        List<FluxTable> all = influxDBProperties.findAll();
//        return Result.success(all);
//    }
//}
//
