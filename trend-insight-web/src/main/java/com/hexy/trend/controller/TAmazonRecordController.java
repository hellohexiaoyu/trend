package com.hexy.trend.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonRecord;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.service.TAmazonRecordService;
import com.hexy.trend.web.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * <p>
 * 更新记录表 前端控制器
 * </p>
 *
 * @author hexy
 * @since 2023-11-02
 */
@Api(tags = "记录查询")
@RestController
@RequestMapping("/tAmazonRecord")
public class TAmazonRecordController {
    @Autowired
    private TAmazonRecordService recordService;
    @ApiOperation(value = "查询更新记录", notes = "查询")
    @PostMapping("/getRecord")
    public Result<TAmazonRecord> getRecord(HttpServletRequest request, @RequestBody @Valid InsightRO.GetRecordRO ro) {

        String value = DepartmentTypeEnum.getDescByValue(ro.getDepartment());

        TAmazonRecord department = recordService.getRecordByDepartment(value,ro.getType());
        return Result.success(department);
    }
}
