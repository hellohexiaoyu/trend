package com.hexy.trend.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.appsecurity.utils.JwtTokenUtils;
import com.hexy.trend.bean.InsightProductRankingImport;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.*;
import com.hexy.trend.enums.AcountTypeEnum;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.enums.DepartmentTypeMonthEnum;
import com.hexy.trend.enums.TypeEnum;
import com.hexy.trend.handler.*;
import com.hexy.trend.service.*;
import com.hexy.trend.service.impl.EasyExcelServiceImpl;
import com.hexy.trend.service.impl.InsightService;
import com.hexy.trend.util.DateUtils;
import com.hexy.trend.util.TrendCommonUtils;
import com.hexy.trend.web.exception.BusinessException;
import com.hexy.trend.web.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hexy
 * @since 2023-09-26
 */
@Api(tags = "月导入-查询")
@Slf4j
@RestController
@RequestMapping("/iprMonth")
public class InsightProductRankingMonthController {
    public static void main(String[] args) throws ParseException {
        String input = "com202310";  // 示例字符串，不区分大小写
        Pattern pattern = Pattern.compile("([A-Z]+)(\\d{6})", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find()) {
            String prefix = matcher.group(1);
            String date = matcher.group(2)+"01";

            System.out.println("前缀部分: " + prefix);
            System.out.println("日期部分: " + date);
        }
    }
    @Autowired
    private EasyExcelServiceImpl excelService;
    @Autowired
    private TAmazonCaMonthService tAmazonCaService;
    @Autowired
    private TAmazonUsaMonthService tAmazonUsaService;
    @Autowired
    private TAmazonDeMonthService tAmazonDeService;
    @Autowired
    private TAmazonEsMonthService tAmazonEsService;
    @Autowired
    private TAmazonFrMonthService tAmazonFrService;
    @Autowired
    private TAmazonItMonthService tAmazonItService;
    @Autowired
    private TAmazonJpMonthService tAmazonJpService;
    @Autowired
    private TAmazonMxMonthService tAmazonMxService;
    @Autowired
    private TAmazonUkMonthService tAmazonUkService;
    @Autowired
    private TAmazonInMonthService tAmazonInService;
    @Autowired
    private TAmazonAuMonthService tAmazonAuService;
    @Autowired
    private TAmazonAeMonthService tAmazonAeService;

    @Autowired
    private TAmazonRecordService tAmazonRecordService;

    @Autowired
    private InsightService insightService;
    private final Pattern pattern = Pattern.compile("([A-Z]+)(\\d{6})", Pattern.CASE_INSENSITIVE);
    @ApiOperation(value = "导入月数据", notes = "导入月数据")
    @PostMapping("/upload")
    public String uploadFile(@RequestPart MultipartFile[] multiFiles,HttpServletRequest request) throws Exception {
        Integer type = (Integer) JwtTokenUtils.getField(request, "type");
        if(type != AcountTypeEnum.ADMIN.getValue()){
            throw new BusinessException("当前账号权限不足");
        }
        log.info("执行业务逻辑-S");
        long begin = System.currentTimeMillis();
        Integer recordId = (Integer) JwtTokenUtils.getField(request, "id");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Map<String,String> map = new HashMap<>();
        for (MultipartFile multiFile : multiFiles) {
            String filename = multiFile.getOriginalFilename();
            int lastDotIndex = filename.lastIndexOf('.');
            String input = filename.substring(0, lastDotIndex);
            // 使用不区分大小写的正则表达式匹配日期部分
//            Pattern pattern = Pattern.compile("([A-Z]+)(\\d{8})", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);

            if (matcher.find()) {
                String prefix = matcher.group(1);
                String date = matcher.group(2)+"01";
                if(!DepartmentTypeEnum.isStringInEnum(prefix)){
                    throw new BusinessException("请检查文件"+ input +"命名规则，例：ca202310");
                }
                System.out.println("前缀部分: " + prefix);
                System.out.println("日期部分: " + date);

                if(map.containsKey(prefix)){
                    throw new BusinessException("同一批次导入不可有重复的部门");
                }
                map.put(prefix,prefix);
            }else{
                throw new BusinessException("请检查文件"+ input +"命名规则，例：ca20231015-20231022");
            }
        }
        map = null;

        // 处理上传的文件
        for (MultipartFile multiFile : multiFiles) {
            // 获取文件名称
            String fileName = multiFile.getOriginalFilename();                               // 获取文件名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));      // 截取后缀名
            File file = File.createTempFile(fileName, suffixName);
            multiFile.transferTo(file);
            /**
             * 美国amazon.com
             * 加拿大 amazon.ca
             * 墨西哥 amazon.com.mx
             * 日本amazon.co.jp
             * 英国amazon.co.uk
             * 德国amazon.co.de
             * 法国站amazon.fr
             * 意大利amazon.it
             * 西班牙amazon.es
             */

            int lastDotIndex = fileName.lastIndexOf('.');
            String input = fileName.substring(0, lastDotIndex);
            Matcher matcher1 = pattern.matcher(input);
            if (matcher1.find()) {
                String name = matcher1.group(1);
                String date = matcher1.group(2)+"01";
                Date parse = dateFormat.parse(date);

                //导入记录非当天数据
                Calendar instance = Calendar.getInstance();
                instance.setTime(parse);
                Calendar instanceNow = Calendar.getInstance();
                instanceNow.setTime(new Date());

                instance.set(Calendar.HOUR_OF_DAY,instanceNow.get(Calendar.HOUR_OF_DAY));
                instance.set(Calendar.MINUTE,instanceNow.get(Calendar.MINUTE));
                instance.set(Calendar.SECOND,instanceNow.get(Calendar.SECOND));
                Date recordTime = instance.getTime();

                log.info("=======>>>>>读取excel："+ fileName);
    //            String name = getBaseName(fileName);
                if(name.equalsIgnoreCase("com")){
                    log.info("美国amazon.com开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonUsaMonthHandler<>(tAmazonUsaService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("美国amazon.com结束");
                } else if (name.equalsIgnoreCase("ca")) {
                    log.info("加拿大 amazon.ca开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonCaMonthHandler<>(tAmazonCaService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("加拿大 amazon.ca结束");
                }else if (name.equalsIgnoreCase("mx")) {
                    log.info("墨西哥 amazon.com.mx开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonMxMonthHandler<>(tAmazonMxService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("墨西哥 amazon.com.mx结束");
                } else if (name.equalsIgnoreCase("jp")) {
                    log.info("日本amazon.co.jp开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonJpMonthHandler<>(tAmazonJpService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("日本amazon.co.jp结束");
                } else if (name.equalsIgnoreCase("uk")) {
                    log.info("英国amazon.co.uk开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonUkMonthHandler<>(tAmazonUkService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("英国amazon.co.uk结束");
                } else if (name.equalsIgnoreCase("de")) {
                    log.info("德国amazon.co.de开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonDeMonthHandler<>(tAmazonDeService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("德国amazon.co.de结束");
                } else if (name.equalsIgnoreCase("fr")) {
                    log.info("法国站amazon.fr开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonFrMonthHandler<>(tAmazonFrService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("法国站amazon.fr结束");
                } else if (name.equalsIgnoreCase("it")) {
                    log.info("意大利amazon.it开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonItMonthHandler<>(tAmazonItService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("意大利amazon.it结束");
                } else if (name.equalsIgnoreCase("es")) {
                    log.info("西班牙amazon.es开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonEsMonthHandler<>(tAmazonEsService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("西班牙amazon.es结束");
                }else if (name.equalsIgnoreCase("in")) {
                    log.info("印度amazon.in开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonInMonthHandler<>(tAmazonInService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("印度amazon.in结束");
                }else if (name.equalsIgnoreCase("au")) {
                    log.info("澳大利亚amazon.au开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonAuMonthHandler<>(tAmazonAuService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("澳大利亚amazon.au结束");
                }else if (name.equalsIgnoreCase("ae")) {
                    log.info("中东amazon.ae开始");
                    excelService.readExcel(file, InsightProductRankingImport.class, new AmazonAeMonthHandler<>(tAmazonAeService,parse));
                    TAmazonRecord tr= new TAmazonRecord();
                    tr.setDepartment(name);
                    tr.setUpdatedAt(recordTime);
                    tr.setUploader(recordId);
                    tr.setType(TypeEnum.Month.getValue());
                    tAmazonRecordService.saveRecord(tr);
                    log.info("中东amazon.ae结束");
                }
            }
            //记录更新日期
            long end = System.currentTimeMillis();
            // 获取文件大小（以字节为单位）
            long fileSizeInBytes = file.length();

            // 将字节转换为MB
            double fileSizeInMB = (double) fileSizeInBytes / (1024 * 1024);
            log.info("插入数据{}MB，耗时：{}ms",fileSizeInMB, end-begin);
        }
        return "ok";
    }
    // 自定义方法以获取文件名中除了扩展名之外的部分
    private static String getBaseName(String fileName) {
        int lastDotIndex = fileName.lastIndexOf('_');
        if (lastDotIndex != -1) {
            return fileName.substring(0, lastDotIndex);
        } else {
            return fileName;
        }
    }

    @ApiOperation(value = "查询", notes = "查询")
    @PostMapping("/getRankingList")
    public Result<Page<T>> getRankingList(HttpServletRequest request, @RequestBody @Valid InsightRO.GetInsightRO ro){
        Integer type = (Integer) JwtTokenUtils.getField(request, "type");
        Integer depart = ro.getDepartment();
        Date date = ro.getDate();
        if(type == AcountTypeEnum.GENERAL.getValue()){
            TrendCommonUtils.setAllFieldsToNull(ro);
            ro.setPageNum(1);
            ro.setPageSize(100);
            ro.setDepartment(depart);
            ro.setDate(date);
        }
        if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_ca.getValue()){
            Page<TAmazonCaMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonCaMonth> rankingList = tAmazonCaService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_de.getValue()){
            Page<TAmazonDeMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonDeMonth> rankingList = tAmazonDeService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_es.getValue()){
            Page<TAmazonEsMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonEsMonth> rankingList = tAmazonEsService.getRankingList(page,ro);
            return Result.success(rankingList);

        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_fr.getValue()){
            Page<TAmazonFrMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonFrMonth> rankingList = tAmazonFrService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_it.getValue()){
            Page<TAmazonItMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonItMonth> rankingList = tAmazonItService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_jp.getValue()){
            Page<TAmazonJpMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonJpMonth> rankingList = tAmazonJpService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_com_mx.getValue()){
            Page<TAmazonMxMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonMxMonth> rankingList = tAmazonMxService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_uk.getValue()){
            Page<TAmazonUkMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonUkMonth> rankingList = tAmazonUkService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_com.getValue()){
            Page<TAmazonUsaMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonUsaMonth> rankingList = tAmazonUsaService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_in.getValue()){
            Page<TAmazonInMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonInMonth> rankingList = tAmazonInService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_au.getValue()){
            Page<TAmazonAuMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonAuMonth> rankingList = tAmazonAuService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_ae.getValue()){
            Page<TAmazonAeMonth> page = new Page<>(ro.getPageNum(),ro.getPageSize());
            Page<TAmazonAeMonth> rankingList = tAmazonAeService.getRankingList(page,ro);
            return Result.success(rankingList);
        }else{
            throw new BusinessException("部门无效");
        }

    }


    @ApiOperation(value = "查询趋势", notes = "查询")
    @PostMapping("/getSortList")
    public Result getSortList(HttpServletRequest request, @RequestBody @Valid InsightRO.GetSortRO ro) {
        Class<?> returnType = null;
        InsightRO.GetInsightDepartmentRO sro = new InsightRO.GetInsightDepartmentRO();
        sro.setSearchTerm(ro.getSearchTerm());

        if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_ca.getValue()){
            returnType = TAmazonCaMonth.class;
            List<TAmazonCaMonth> rankingListForQS = tAmazonCaService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);

        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_de.getValue()){
            returnType = TAmazonDeMonth.class;
            List<TAmazonDeMonth> rankingListForQS = tAmazonDeService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_es.getValue()){
            returnType = TAmazonEsMonth.class;
            List<TAmazonEsMonth> rankingListForQS = tAmazonEsService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_fr.getValue()){
            returnType = TAmazonFrMonth.class;
            List<TAmazonFrMonth> rankingListForQS = tAmazonFrService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_it.getValue()){
            returnType = TAmazonItMonth.class;
            List<TAmazonItMonth> rankingListForQS = tAmazonItService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_jp.getValue()){
            returnType = TAmazonJpMonth.class;
            List<TAmazonJpMonth> rankingListForQS = tAmazonJpService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);

        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_com_mx.getValue()){
            returnType = TAmazonMxMonth.class;
            List<TAmazonMxMonth> rankingListForQS = tAmazonMxService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_co_uk.getValue()){
            returnType = TAmazonUkMonth.class;
            List<TAmazonUkMonth> rankingListForQS = tAmazonUkService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_com.getValue()){
            returnType = TAmazonUsaMonth.class;
            List<TAmazonUsaMonth> rankingListForQS = tAmazonUsaService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_in.getValue()){
            returnType = TAmazonInMonth.class;
            List<TAmazonInMonth> rankingListForQS = tAmazonInService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_au.getValue()){
            returnType = TAmazonAuMonth.class;
            List<TAmazonAuMonth> rankingListForQS = tAmazonAuService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else if(ro.getDepartment() == DepartmentTypeMonthEnum.amazon_ae.getValue()){
            returnType = TAmazonAeMonth.class;
            List<TAmazonAeMonth> rankingListForQS = tAmazonAeService.getRankingListForQS(sro);
            return Result.success(rankingListForQS);
        }else {
            throw new BusinessException("部门无效");
        }

//        String departments =  DepartmentTypeMonthEnum.getDescByValue(ro.getDepartment());
//        List<?> sortList = insightService.getSortList(ro.getSearchTerm(), departments, returnType);
//        return Result.success(sortList);
    }
}

