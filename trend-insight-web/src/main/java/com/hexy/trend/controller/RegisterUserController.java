package com.hexy.trend.controller;


import com.hexy.trend.bean.aci.UserCI;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.bean.aro.UserRO;
import com.hexy.trend.entity.TAmazonRecord;
import com.hexy.trend.entity.TAmazonUser;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.mail.MailMessage;
import com.hexy.trend.mail.MailSender;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.security.UserDetails;
import com.hexy.trend.service.TAmazonUserService;
import com.hexy.trend.util.TrendCommonUtils;
import com.hexy.trend.web.exception.BusinessException;
import com.hexy.trend.web.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
@Api(tags = "用户注册")
@Slf4j
@RestController
@RequestMapping("/register")
public class RegisterUserController {

    @Autowired
    private TAmazonUserService user;

    @Resource
    private MailSender mailSender;

    @Autowired
    private RedisUtil redisUtil;
    @ApiOperation(value = "注册", notes = "注册")
    @PostMapping("/add")
    public Result<TAmazonUser> add(HttpServletRequest request, @RequestBody @Valid UserRO.AddUserRO ro) {
        TAmazonUser convert = UserCI.INSTANCE.convert(ro);
        //验证码验证
        String username = ro.getUsername();
        String s = redisUtil.get(username);
        if(null == s){
            throw new BusinessException("验证码已过期");
        }
        //验证密码两次输入是否一致
        if(!ro.getPassword().equals(ro.getConfirmPassword())){
            throw new BusinessException("输入密码不一致");
        }
        Integer integer = user.saveUser(convert);
        return Result.success(convert);
    }

    @ApiOperation(value = "账号验证（是否已存在）", notes = "账号验证（是否已存在）")
    @PostMapping("/check")
    public Result<TAmazonUser> check(HttpServletRequest request, @RequestBody @Valid UserRO.CheckUserRO ro) {
        TAmazonUser userEntity = user.getUserByUsername(ro.getUsername());
        if(null != userEntity){
            throw new BusinessException("当前账号已注册");
        }
        return Result.success();
    }

    @ApiOperation(value = "发送验证码", notes = "发送验证码")
    @PostMapping("/sendMail")
    public Result<TAmazonUser> sendMail(HttpServletRequest request, @RequestBody @Valid UserRO.SendMailRO ro) {
        if(null != redisUtil.get(ro.getEmail())){
            throw new BusinessException("请勿重复发送验证码,如未收到验证码,请三分钟后再次发送");
        }
        String randomCode = TrendCommonUtils.generateRandomCode();
        log.info("=====randomCode: " + randomCode);
        String content = "【亚马逊关键排名检索引擎】 验证码 "+ randomCode +" 用于账号注册验证，3分钟内有效，请勿泄露和转发。如非本人操作，请忽略此邮件。";
        MailMessage mailMessage = MailMessage.builder()
                .receiver(ro.getEmail())
                .subject("亚马逊关键排名检索引擎")
                .content(content)
//                .annexFiles(Collections.singletonList(new MailMessage.AnnexFileInfo("测试附件.txt", "Hello World".getBytes())))
                .build();
        try {
            mailSender.sendEmail(mailMessage);
        } catch (IOException | MessagingException e) {
            // 异常
            throw new RuntimeException(e);
        }
        //验证码缓存到redis
        redisUtil.setNx(ro.getEmail(),randomCode,3L, TimeUnit.MINUTES);
        return Result.success();
    }
}

