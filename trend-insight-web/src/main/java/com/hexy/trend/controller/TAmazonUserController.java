package com.hexy.trend.controller;


import com.hexy.trend.security.UserDetails;
import com.hexy.trend.web.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
@Api(tags = "用户登录接口")
@Controller
public class TAmazonUserController {
    @ApiOperation(value = "登录", notes = "登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true),
            @ApiImplicitParam(name = "password", value = "密码", required = true),
//            @ApiImplicitParam(name = "loginType", value = "登录方式标记", required = true),
    })
    @PostMapping("/login")
    public Result<UserDetails> login(@NotBlank String username, @NotBlank String password) {
        // 这里面不需要写任何代码，单纯为了在swagger页面上可以展示出此接口
        // 并且代码也完全不会执行到这里，已经被security拦截了
        // 真正的逻辑由UserDeatilsService处理
        return null;
    }


    @ApiOperation(value = "登出", notes = "登出")
    @ApiImplicitParams({})
    @PostMapping("/logout")
    public void logout() {
        // 这里面不需要写任何代码，单纯为了在swagger页面上可以展示出此接口
        // 并且代码也完全不会执行到这里，已经被security拦截了
    }
}

