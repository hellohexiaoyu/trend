package com.hexy.trend.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
@Data
public class TAmazonCaWeek implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String department;

    private String searchTerm;
    private String translate;
    private Long dayRank;

    private Long dayFloat;

    private Long yesterdayRank;

    private Long yesterdayFloat;

    private Long previousRank;

    private Long previousFloat;

    private Date date;

    private Date createdAt;

    private Date updatedAt;

    @TableField(exist = false)
    private List<TAmazonCaWeek> sort;

}
