package com.hexy.trend.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
    /**
     * 日：1 一：2 .。。  六：7
     * @param date
     * @return
     */
    public static int getDayOfWeek(Date date) {
        // 使用 Calendar 获取星期几
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }
}
