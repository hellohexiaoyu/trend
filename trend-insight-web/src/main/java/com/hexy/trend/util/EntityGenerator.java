//package com.hexy.trend.util;
//
//import com.baomidou.mybatisplus.annotation.DbType;
//import com.baomidou.mybatisplus.annotation.IdType;
//import com.baomidou.mybatisplus.generator.AutoGenerator;
//import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
//import com.baomidou.mybatisplus.generator.config.GlobalConfig;
//import com.baomidou.mybatisplus.generator.config.PackageConfig;
//import com.baomidou.mybatisplus.generator.config.StrategyConfig;
//import com.baomidou.mybatisplus.generator.config.rules.DateType;
//import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
//import org.apache.poi.poifs.nio.DataSource;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
////public static final String url = "jdbc:mysql://127.0.0.1:3306/trend_insight_dev?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&serverTimezone=Asia/Shanghai";
//@RunWith(SpringJUnit4ClassRunner.class)
////@ContextConfiguration("classpath:application-dev.yml")
//public class EntityGenerator {
//
//    private static final String url = "jdbc:mysql://127.0.0.1:3306/trend_insight_dev" +
//            "?useUnicode=true&characterEncoding=utf8" +
//            "&allowPublicKeyRetrieval=True&useSSL=false" +
//            "&serverTimezone=Asia/Shanghai";
//    private static final String userName    = "root";
//    private static final String password    = "root";
//    private static final String tableName   = "product_ranking_three";
////    private static final String tablePrefix = "t_";
//
//    @Test
//    public void testGenerator() {
//        // 全局配置
//        GlobalConfig config = new GlobalConfig();
//        config.setActiveRecord(false) // AR模式
//                .setAuthor("hexy")    // 作者
//                .setOutputDir(System.getProperty("user.dir") + "/src/main/java") // 生成路径
//                .setFileOverride(true) // 文件覆盖
//                .setIdType(IdType.AUTO) // 主键策略
//                .setServiceName("%sService") // 设置生成的service接口的名字，默认为I%sSerice
//                .setDateType(DateType.ONLY_DATE);
//
//
//        // 数据源配置
//        DataSourceConfig dsConfig = new DataSourceConfig();
//        dsConfig.setDbType(DbType.MYSQL)     // 设置数据库类型
//                .setDriverName("com.mysql.cj.jdbc.Driver")
//                .setUrl("jdbc:mysql://127.0.0.1:3306/trend_insight_dev?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&serverTimezone=Asia/Shanghai")
//                .setUsername("root")
//                .setPassword("root");
//
//        // 策略配置
//        StrategyConfig stConfig = new StrategyConfig();
//        stConfig.setCapitalMode(true) // 全局大写命名
//                .setNaming(NamingStrategy.underline_to_camel); // 数据库表映射到实体的命名策略
//
//        // 包名配置
//        PackageConfig pkConfig = new PackageConfig();
//        pkConfig.setParent("com.hexy.trend.temp")
//                .setMapper("dao")
//                .setService("service")
//                .setController("controller")
//                .setEntity("entity")
//                .setXml("dao.mapper");
//
//        //  整合
//        AutoGenerator ag = new AutoGenerator();
//        ag.setGlobalConfig(config).setDataSource(dsConfig).setStrategy(stConfig).setPackageInfo(pkConfig);
//        // 生成
//        ag.execute();
//    }
//}