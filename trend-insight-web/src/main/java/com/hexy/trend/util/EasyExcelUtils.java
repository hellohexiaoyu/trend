package com.hexy.trend.util;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;

import com.hexy.trend.bean.ExcelParams;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

@Slf4j
public class EasyExcelUtils {
    private static final String SUFFIX_XLSX = ".xlsx";
    private static final String SUFFIX_XLS=".xls";

    public static boolean isExcel(File file) {
        String fileName = file.getName();

        if (file.isFile()) {
            int idx = fileName.lastIndexOf(".");
            if (idx > 0) {
                String ext = fileName.substring(fileName.lastIndexOf("."), fileName.length());
                if (".xls".equals(ext) || ".xlsx".equals(ext)) {
                    return true;
                }
            }
        }

        return false;
    }
    public static <T> void readExcel(ExcelParams<T> params) {
        ExcelReader excelReader = null;
        ReadSheet readSheet1 = null;
        try {
            if (params.getFileSrc() == null) {
                log.error("Excel读取失败，fileSrc为空！");
                return;
            }

            excelReader = EasyExcel.read(params.getFileSrc()).autoTrim(true).build();

            // 注册监听器
            if (params.getListener() != null) {
                readSheet1 = EasyExcel.readSheet(0).head(params.getDataClass()).registerReadListener(params.getListener()).build();
            } else {
                readSheet1 = EasyExcel.readSheet(0).head(params.getDataClass()).build();
            }

            excelReader.read(readSheet1);

        } finally {
            if (excelReader != null) {
                //关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }

    /**
     * Excel写出
     * @param params
     */
    public static <T> void writeExcel(ExcelParams<T> params) {

        ExcelWriterSheetBuilder writerSheetBuilder = null;
        ExcelWriter excelWriter = null;
        try {

            if (params.getFileDest() == null) {
                log.error("Excel写出失败，fileDest为空！");
                return;
            }

            excelWriter = EasyExcel.write(params.getFileDest(), params.getDataClass()).build();

//            FreezePane fp = params.getFreezePane();

            writerSheetBuilder = EasyExcel.writerSheet(params.getSheetName());

//            if (fp != null) {
//                writerSheetBuilder.registerWriteHandler(new SheetFreezeWriteHandler(fp));
//            }

            WriteSheet writeSheet = writerSheetBuilder.build();
            excelWriter.write(params.getDataList(), writeSheet);
        } finally {
            if (excelWriter != null) {
                excelWriter.finish();
            }
        }
    }
}
