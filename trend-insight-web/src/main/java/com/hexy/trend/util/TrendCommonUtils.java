package com.hexy.trend.util;

import java.lang.reflect.Field;
import java.util.Random;

public class TrendCommonUtils {
    /**
     * 六位随机码
     * @return
     */
    public static String generateRandomCode() {
        // 定义验证码长度
        int codeLength = 6;

        // 定义验证码字符集
        String codeChars = "0123456789";

        // 使用Random类生成随机数
        Random random = new Random();

        // 生成随机验证码
        StringBuilder code = new StringBuilder(codeLength);
        for (int i = 0; i < codeLength; i++) {
            // 从字符集中随机选择一个字符
            char randomChar = codeChars.charAt(random.nextInt(codeChars.length()));
            code.append(randomChar);
        }
        return code.toString();
    }

    public static void setAllFieldsToNull(Object obj) {
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                field.set(obj, null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
