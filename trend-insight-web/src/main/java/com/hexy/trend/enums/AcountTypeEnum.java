package com.hexy.trend.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum AcountTypeEnum {

    ADMIN(0, "管理员"),

    GENERAL(1, "普通用户"),
    VIP(2, "VIP");

    private final Integer value;

    private final String desc;
}
