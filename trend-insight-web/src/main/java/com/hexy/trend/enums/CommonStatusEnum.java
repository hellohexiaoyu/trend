package com.hexy.trend.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum CommonStatusEnum {

    DISABLE(0, "停用"),

    ENABLE(1, "启用");

    private final Integer value;

    private final String desc;
}
