package com.hexy.trend.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum TypeEnum {

    Day(1, "天"),

    Week(2, "周"),
    Month(3, "月");

    private final Integer value;

    private final String desc;
}
