package com.hexy.trend.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum DepartmentTypeEnum {
    /**
     * 美国amazon.com
     * 加拿大 amazon.ca
     * 墨西哥 amazon.com.mx
     * 日本amazon.co.jp
     * 英国amazon.co.uk
     * 德国amazon.co.de
     * 法国站amazon.fr
     * 意大利amazon.it
     * 西班牙amazon.es
     */
    amazon_com(0, "com"),

    amazon_ca(1,"ca"),
    amazon_com_mx(2,"mx"),
    amazon_co_jp(3,"jp"),
    amazon_co_uk(4,"uk"),
    amazon_co_de(5,"de"),
    amazon_fr(6,"fr"),
    amazon_it(7,"it"),
    amazon_es(8,"es"),

    amazon_in(9,"in"),
    amazon_au(10,"au"),
    amazon_ae(11,"ae");

    private final Integer value;

    private final String desc;

    // 静态方法，根据枚举值查找描述
    public static String getDescByValue(Integer value) {
        for (DepartmentTypeEnum marketplace : DepartmentTypeEnum.values()) {
            if (marketplace.value.equals(value)) {
                return marketplace.desc;
            }
        }
        return null; // 或者返回一个默认值或抛出异常，具体取决于你的需求
    }
    public static boolean isStringInEnum(String input) {
        for (DepartmentTypeEnum department : DepartmentTypeEnum.values()) {
            if (department.getDesc().equals(input)) {
                return true;
            }
        }
        return false;
    }
}
