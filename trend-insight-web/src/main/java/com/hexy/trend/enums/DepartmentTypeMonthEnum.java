package com.hexy.trend.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum DepartmentTypeMonthEnum {
    /**
     * 美国amazon.com
     * 加拿大 amazon.ca
     * 墨西哥 amazon.com.mx
     * 日本amazon.co.jp
     * 英国amazon.co.uk
     * 德国amazon.co.de
     * 法国站amazon.fr
     * 意大利amazon.it
     * 西班牙amazon.es
     */
    amazon_com(0, "com_month"),

    amazon_ca(1,"ca_month"),
    amazon_com_mx(2,"mx_month"),
    amazon_co_jp(3,"jp_month"),
    amazon_co_uk(4,"uk_month"),
    amazon_co_de(5,"de_month"),
    amazon_fr(6,"fr_month"),
    amazon_it(7,"it_month"),
    amazon_es(8,"es_month"),
    amazon_in(9,"in_month"),

    amazon_au(10,"au_month"),
    amazon_ae(11,"ae_month");

    private final Integer value;

    private final String desc;

    // 静态方法，根据枚举值查找描述
    public static String getDescByValue(Integer value) {
        for (DepartmentTypeMonthEnum marketplace : DepartmentTypeMonthEnum.values()) {
            if (marketplace.value.equals(value)) {
                return marketplace.desc;
            }
        }
        return null; // 或者返回一个默认值或抛出异常，具体取决于你的需求
    }
    public static boolean isStringInEnum(String input) {
        for (DepartmentTypeMonthEnum department : DepartmentTypeMonthEnum.values()) {
            if (department.getDesc().equals(input)) {
                return true;
            }
        }
        return false;
    }
}
