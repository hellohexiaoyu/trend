package com.hexy.trend.handler;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.hexy.trend.entity.TAmazonJpMonth;
import com.hexy.trend.service.TAmazonJpMonthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;

import java.text.SimpleDateFormat;
import java.util.*;


@Slf4j
public class AmazonJpMonthHandler<T> extends AnalysisEventListener<T> {
    private TAmazonJpMonthService tAmazonCaService;
    private Date date;
    private List<TAmazonJpMonth> dataList = new ArrayList<>();
    List<TAmazonJpMonth> yesterdayList  = new ArrayList<>();
    List<TAmazonJpMonth> previousList = new ArrayList<>();
    public static final int CACHE_SIZE = 10000;

    public AmazonJpMonthHandler(TAmazonJpMonthService tAmazonCaService, Date date){
        this.tAmazonCaService = tAmazonCaService;
        this.date = date;
    }
    @Override
    public void invoke(Object data, AnalysisContext analysisContext) {
        //数据存储到list，供批量处理，或后续自己业务逻辑处理。
        TAmazonJpMonth three = new TAmazonJpMonth();
        BeanUtils.copyProperties(data,three);
        dataList.add(three);

        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (dataList.size() >= CACHE_SIZE) {
            processData();
            // 存储完成清理 list
            dataList.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//        log.info("数据解析完成{}",dataList.toString());
        processData();
        dataList.clear();
//        log.info("数据解析完成{}",dataList.toString());
    }
    public void upHandleBusinessLogic( List<TAmazonJpMonth> updatedList) {
        log.info("执行业务逻辑-S");
        long begin = System.currentTimeMillis();
        tAmazonCaService.updateBatch(updatedList);
        long end = System.currentTimeMillis();
        log.info("使用原生jdbc插入数据{}条，耗时：{}ms",dataList.size(),end-begin);
        log.info("执行业务逻辑-E");
    }
    public void inHandleBusinessLogic( List<TAmazonJpMonth> insertedList) {
        log.info("执行业务逻辑-S");
        long begin = System.currentTimeMillis();
        tAmazonCaService.addBatch(insertedList);
        long end = System.currentTimeMillis();
        log.info("使用原生jdbc插入数据{}条，耗时：{}ms",dataList.size(),end-begin);
        log.info("执行业务逻辑-E");
    }

    public  void processData(){
        // 创建日期格式化对象，将日期格式化到年-月-日
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(null != dataList && dataList.size() >0){
            //查询全部记录
            //获取日期
//            Date day = dataList.get(0).getDate();
            Date day = date;
            Calendar calendar = Calendar.getInstance();
            // 获取后一天
            calendar.setTime(day);

            // 获取后一个月的一号
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date nextDay = calendar.getTime();

            // 获取前一个月的一号
            calendar.add(Calendar.MONTH, -2);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            Date yesterdayDay = calendar.getTime();

            if(null == yesterdayList || yesterdayList.size() ==0) {
                yesterdayList = tAmazonCaService.getAmazonCaList(yesterdayDay);
            }
            if(null == previousList || previousList.size() == 0) {
                previousList = tAmazonCaService.getAmazonCaList(nextDay);
            }
            // 创建用于存储更新和插入的集合（中间日期插入时更新后台数据）
            List<TAmazonJpMonth> updatedNextDayList = new ArrayList<>();


            //加快查询速度
            Map<String, TAmazonJpMonth> insertDayMap = new HashMap<>();
            for (TAmazonJpMonth item : dataList) {
                //没有前一天数据
                if(null == yesterdayList || yesterdayList.size() == 0){
                    item.setYesterdayRank(item.getDayRank());
                    item.setPreviousRank(item.getDayRank());
                    item.setDayFloat(0L);
                    item.setYesterdayFloat(0L);
                    item.setPreviousFloat(0L);
                }
                item.setDate(date);
                insertDayMap.put(item.getSearchTerm(), item);
            }
            //处理前一天数据
            Iterator<TAmazonJpMonth> yesterday = yesterdayList.iterator();
            while (yesterday.hasNext()) {

                //昨天
                TAmazonJpMonth item = yesterday.next();
                String searchTerm = item.getSearchTerm();

                //今天
                TAmazonJpMonth insertDayItem = insertDayMap.get(searchTerm);


                if (insertDayItem != null) {
                    //记录更新数据
                    //计算今日数据 三日内涨幅
                    //前天没数据时取今天数据的记录
                    if(null != item.getDayRank()){
                        insertDayItem.setYesterdayRank(item.getDayRank());
                        long dayFloat = insertDayItem.getDayRank()-insertDayItem.getYesterdayRank();
                        insertDayItem.setDayFloat(dayFloat);
                    }else{
                        insertDayItem.setYesterdayRank(insertDayItem.getDayRank());
                        long dayFloat = insertDayItem.getDayRank() - insertDayItem.getYesterdayRank();
                        insertDayItem.setDayFloat(dayFloat);
                    }

                    //前天没数据时取今天数据的记录
                    if(null != item.getYesterdayRank()){
                        insertDayItem.setPreviousRank(item.getYesterdayRank());
                        long yesterdayFloat =  insertDayItem.getYesterdayRank()-insertDayItem.getPreviousRank();
                        insertDayItem.setYesterdayFloat(yesterdayFloat);
                    }else{
                        if(null != insertDayItem.getYesterdayRank()){
                            insertDayItem.setPreviousRank(insertDayItem.getYesterdayRank());
                        }else{
                            insertDayItem.setPreviousRank(insertDayItem.getDayRank());
                        }
                        long yesterdayFloat = insertDayItem.getYesterdayRank()- insertDayItem.getPreviousRank();
                        insertDayItem.setYesterdayFloat(yesterdayFloat);
                    }

                    long PreviousFloat;
                    if(null != item.getPreviousRank()){
                        PreviousFloat = insertDayItem.getPreviousRank()-item.getPreviousRank();
                    }else{
                        if(null != item.getYesterdayRank()){
                            PreviousFloat = insertDayItem.getPreviousRank()-item.getYesterdayRank();
                        }else{
                            PreviousFloat = insertDayItem.getPreviousRank()-item.getDayRank();
                        }

                    }
                    insertDayItem.setPreviousFloat(PreviousFloat);


                    //修改后放回map中
                    insertDayMap.put(searchTerm,insertDayItem);

                    //移除已匹配数据
                    yesterday.remove();
                }
            }

            //处理后一天数据
            Iterator<TAmazonJpMonth> next = previousList.iterator();
            while (next.hasNext()) {

                //后一天数据
                TAmazonJpMonth item = next.next();
                String searchTerm = item.getSearchTerm();
                //今天数据
                TAmazonJpMonth insertDayItem = insertDayMap.get(searchTerm);
                if (insertDayItem != null) {
                    item.setYesterdayRank(insertDayItem.getDayRank());
                    long dayFloat = item.getDayRank() - item.getYesterdayRank();
                    item.setDayFloat(dayFloat);
                    if(null != insertDayItem.getYesterdayRank()){
                        item.setPreviousRank(insertDayItem.getYesterdayRank());
                        long yesterdayFloat = item.getYesterdayRank() - item.getPreviousRank();
                        item.setYesterdayFloat(yesterdayFloat);
                    }else{
                        item.setPreviousRank(insertDayItem.getDayRank());
                        long yesterdayFloat = item.getYesterdayRank() - item.getPreviousRank();
                        item.setYesterdayFloat(yesterdayFloat);
                    }
                    if(null == insertDayItem.getPreviousRank()){
                        item.setPreviousFloat(0L);
                    }else{
                        long previousFloat = item.getPreviousRank() -insertDayItem.getPreviousRank();
                        item.setPreviousFloat(previousFloat);
                    }

                    updatedNextDayList.add(item);
                    next.remove();
                }
            }

            if(null != updatedNextDayList && updatedNextDayList.size() >0){
                //更新后天数据
                upHandleBusinessLogic(updatedNextDayList);
                updatedNextDayList = null;
            }

            List<TAmazonJpMonth> insertDayList = new ArrayList<>(insertDayMap.values());
            inHandleBusinessLogic(insertDayList);
            insertDayMap = null;
            insertDayList = null;
        }
    }
}
