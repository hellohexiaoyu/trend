package com.hexy.trend.handler;

import com.alibaba.excel.write.handler.AbstractRowWriteHandler;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.metadata.holder.WriteTableHolder;
import com.alibaba.excel.write.metadata.holder.WriteWorkbookHolder;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import org.apache.poi.ss.usermodel.*;

public class AutoNoHandler extends AbstractRowWriteHandler {
    private int rowNum;
    @Override
    public void beforeRowCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Integer rowIndex, Integer relativeRowIndex, Boolean isHead) {
    }
    @Override
    public void afterRowCreate(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row, Integer relativeRowIndex, Boolean isHead) {


        WriteCellStyle headWriteCellStyle = new WriteCellStyle();

        WriteFont headWriteFont = new WriteFont();
        // 设置表头水平垂直居中
        headWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);


        WriteWorkbookHolder parentWriteWorkbookHolder = writeSheetHolder.getParentWriteWorkbookHolder();
        //背景设置
        CellStyle cellStyle = parentWriteWorkbookHolder.createCellStyle(headWriteCellStyle,null);
        cellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        Font font = parentWriteWorkbookHolder.createFont(headWriteFont, null, false);
        //字体设置
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        cellStyle.setFont(font);
        // 设置表头边框样式
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);

        if (rowNum == 0) {
            Cell cell = row.createCell(0);
            cell.setCellValue("序号");
            cell.setCellStyle(cellStyle);
        } else {
            Cell cell = row.createCell(0);
            cell.setCellValue(rowNum);
        }
        rowNum++;
    }
    @Override
    public void afterRowDispose(WriteSheetHolder writeSheetHolder, WriteTableHolder writeTableHolder, Row row, Integer relativeRowIndex, Boolean isHead) {
    }
}

