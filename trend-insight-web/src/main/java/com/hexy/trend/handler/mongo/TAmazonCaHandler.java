//package com.hexy.trend.handler.mongo;
//
//import com.hexy.trend.bean.aro.TAmazonCaPageRO;
//import com.hexy.trend.bean.aro.TAmazonCaRO;
//import com.hexy.trend.entity.TAmazonCa;
//import com.hexy.trend.mongodb.core.entity.Page;
//import com.hexy.trend.mongodb.core.wrapper.LambdaQueryWrapper;
//import com.hexy.trend.mongodb.core.wrapper.Wrappers;
//import com.hexy.trend.servicemongo.TAmazonCaMongoService;
//import com.hexy.trend.util.ParamsUtil;
//import com.hexy.trend.web.result.Result;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.Objects;
//
///**
// * 测试数据 处理器
// *
// * @author hexy
// * @date 2023-06-13
// */
//@Component
//public class TAmazonCaHandler {
//
//    @Autowired
//    private TAmazonCaMongoService service;
//
//    /**
//     * 新增测试数据
//     */
//    public Result<Boolean> save(TAmazonCaRO req) {
//
//        TAmazonCa save = ParamsUtil.copyProperties(req, TAmazonCa.class);
//        return Result.success(service.save(save));
//
//    }
//
//    /**
//     * 修改测试数据
//     */
//    public Result<Boolean> update(TAmazonCaRO req) {
//
//        TAmazonCa update = ParamsUtil.copyProperties(req, TAmazonCa.class);
//        return Result.success(service.updateById(update));
//
//    }
//
//    /**
//     * 通过id删除测试数据
//     */
//    public Result<Boolean> deleteById(String id) {
//
//        return Result.success(service.removeById(id));
//
//    }
//
//    /**
//     * 通过id获取测试数据
//     */
//    public Result<TAmazonCa> getById(String id) {
//
//        TAmazonCa dbData = service.getById(id);
//        if (Objects.isNull(dbData)) {
//            return Result.success();
//        }
//        return Result.success(ParamsUtil.copyProperties(dbData, TAmazonCa.class));
//
//    }
//
//    /**
//     * 分页获取测试数据
//     */
//    public Result<Page<TAmazonCa>> queryList(TAmazonCaPageRO req) {
//
//        LambdaQueryWrapper<TAmazonCa> query = Wrappers.<TAmazonCa>lambdaQuery()
//                .eq(Objects.nonNull(req.getDate()), TAmazonCa::getDate, req.getDate())
//                .between((Objects.nonNull(req.getStartTime()) && Objects.nonNull(req.getEndTime())), TAmazonCa::getCreatedAt, req.getStartTime(), req.getEndTime());
//        Page<TAmazonCa> page = service.page(query, req.getPageNo(), req.getPageSize());
//        return Result.success(page);
//
//    }
//
//}
