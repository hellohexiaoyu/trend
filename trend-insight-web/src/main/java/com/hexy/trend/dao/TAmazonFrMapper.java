package com.hexy.trend.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hexy.trend.entity.TAmazonFr;
import com.hexy.trend.entity.TAmazonIt;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonFrMapper extends RootMapper<TAmazonFr> {
    int insertBatchSomeColumn(List<TAmazonFr> entityList);
    int updateBatch(@Param("list") List<TAmazonFr> entityList);
    @Select("CREATE TEMPORARY TABLE TempTableToDelete AS " +
            "SELECT date, search_term, MAX(id) AS max_id " +
            "FROM t_amazon_fr " +
            "GROUP BY date, search_term")
    void createTempTableToDelete();

    @Delete("DELETE FROM t_amazon_fr WHERE id NOT IN (SELECT max_id FROM TempTableToDelete)")
    void deleteRecordsBasedOnTempTable();

    @Select("DROP TEMPORARY TABLE TempTableToDelete")
    void dropTempTable();

    @Delete("delete from t_amazon_fr where date < #{cutoffDate}")
    void delData(@Param("cutoffDate") Date cutoffDate);
}
