package com.hexy.trend.dao;

import com.hexy.trend.entity.TAmazonCaWeek;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonCaWeekMapper extends RootMapper<TAmazonCaWeek> {
    int insertBatchSomeColumn(List<TAmazonCaWeek> entityList);
    int updateBatch(@Param("list") List<TAmazonCaWeek> entityList);

    @Select("CREATE TEMPORARY TABLE TempTableToDelete AS " +
            "SELECT date, search_term, MAX(id) AS max_id " +
            "FROM t_amazon_ca_week " +
            "GROUP BY date, search_term")
    void createTempTableToDelete();

    @Delete("DELETE FROM t_amazon_ca_week WHERE id NOT IN (SELECT max_id FROM TempTableToDelete)")
    void deleteRecordsBasedOnTempTable();

    @Select("DROP TEMPORARY TABLE TempTableToDelete")
    void dropTempTable();
}
