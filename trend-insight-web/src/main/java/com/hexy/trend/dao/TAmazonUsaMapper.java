package com.hexy.trend.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonUsa;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2023-10-13
 */
public interface TAmazonUsaMapper extends RootMapper<TAmazonUsa>  {
    int insertBatchSomeColumn(List<TAmazonUsa> entityList);
    int updateBatch(@Param("list") List<TAmazonUsa> entityList);

    @Select("CREATE TEMPORARY TABLE TempTableToDelete AS " +
            "SELECT date, search_term, MAX(id) AS max_id " +
            "FROM t_amazon_usa " +
            "GROUP BY date, search_term")
    void createTempTableToDelete();

    @Delete("DELETE FROM t_amazon_usa WHERE id NOT IN (SELECT max_id FROM TempTableToDelete)")
    void deleteRecordsBasedOnTempTable();

    @Select("DROP TEMPORARY TABLE TempTableToDelete")
    void dropTempTable();
    @Delete("delete from t_amazon_usa where date < #{cutoffDate}")
    void delData(@Param("cutoffDate") Date cutoffDate);
}
