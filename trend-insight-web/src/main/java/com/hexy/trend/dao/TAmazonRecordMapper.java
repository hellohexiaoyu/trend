package com.hexy.trend.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hexy.trend.entity.TAmazonRecord;

/**
 * <p>
 * 更新记录表 Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2023-11-02
 */
public interface TAmazonRecordMapper extends BaseMapper<TAmazonRecord> {

}
