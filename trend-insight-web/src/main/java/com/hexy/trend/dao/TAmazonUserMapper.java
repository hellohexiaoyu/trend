package com.hexy.trend.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hexy.trend.entity.TAmazonUser;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
public interface TAmazonUserMapper extends BaseMapper<TAmazonUser> {

}
