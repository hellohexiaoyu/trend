package com.hexy.trend.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hexy.trend.entity.TAmazonAu;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAuMapper extends BaseMapper<TAmazonAu> {
    int insertBatchSomeColumn(List<TAmazonAu> entityList);
    int updateBatch(@Param("list") List<TAmazonAu> entityList);

    @Select("CREATE TEMPORARY TABLE TempTableToDelete AS " +
            "SELECT date, search_term, MAX(id) AS max_id " +
            "FROM t_amazon_au " +
            "GROUP BY date, search_term")
    void createTempTableToDelete();

    @Delete("DELETE FROM t_amazon_au WHERE id NOT IN (SELECT max_id FROM TempTableToDelete)")
    void deleteRecordsBasedOnTempTable();

    @Select("DROP TEMPORARY TABLE TempTableToDelete")
    void dropTempTable();


    @Delete("delete from t_amazon_au where date < #{cutoffDate}")
    void delData(@Param("cutoffDate") Date cutoffDate);
}
