package com.hexy.trend.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hexy.trend.appsecurity.base.JwtUserDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Data
public class UserDetails implements JwtUserDetails {

//    @ApiModelProperty(value = "登录标记（0：医生/专家  1:学员）")
//    private String loginType;
    @ApiModelProperty(value = "用户ID")
    private Integer id;

    @ApiModelProperty(value = "用户名")
    private String username;
    @JsonIgnore
    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "性别(0:女 1男)")
    private Integer sex;

    @ApiModelProperty(value = "电话")
    private String tel;

    @ApiModelProperty(value = "地址")
    private String addr;

    @ApiModelProperty(value = "身份证号")
    private String card;

    @ApiModelProperty(value = "头像")
    private String avatar;

    @ApiModelProperty(value = "类型（0：管理员 1：普通 2：Vip）")
    private Integer type;

    @ApiModelProperty(value = "vip开始时间")
    private Date startTime;
    @ApiModelProperty(value = "vip结束时间")
    private Date endTime;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }



    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    // 需要保存到token中的字段，从这里处理
    @Override
    public Map<String, Object> toClaimsMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("username", username);
        map.put("tel", tel);
        map.put("type", type);
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        return map;
    }
}