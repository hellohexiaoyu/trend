package com.hexy.trend.security;

import com.hexy.trend.bean.aci.UserCI;
import com.hexy.trend.entity.TAmazonUser;
import com.hexy.trend.service.TAmazonUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private TAmazonUserService service;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        // 查询用户
        TAmazonUser user = service.getUserByUsername(username);

        // 用户存在
        if (user != null) {
            return UserCI.INSTANCE.convert(user);
        }

        // 用户不在
        else {
            throw new UsernameNotFoundException(username);
        }
    }
}