package com.hexy.trend.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collection;


@Component
public class TrendAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserDetailsService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

//        MyAuthenticationToken auth = (MyAuthenticationToken) authentication;
        // 字段解析
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
//        String loginType = auth.getLoginType();
        String encoded = "";
        // 查询用户
        UserDetails userDetails = userService.loadUserByUsername(username);

//        if(userDetails.getStatus().equals(CommonStatusEnum.DISABLE.getValue())){
//            throw new DisabledException("当前账户已停用，请联系管理员");
//        }
//        userDetails.setCustomerId(1);

        // 密码校验
        encoded = passwordEncoder.encode(password);
        if (!userDetails.getPassword().equals(encoded)) {
            throw new BadCredentialsException(username);
        }
        // 权限列表
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();

        // 返回结果
        return new UsernamePasswordAuthenticationToken(userDetails, password, authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

}
