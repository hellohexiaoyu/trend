package com.hexy.trend.security;

import com.hexy.trend.appsecurity.entrypoint.MyAuthenticationEntryPoint;
import com.hexy.trend.appsecurity.filter.MyJwtAuthenticationFilter;
import com.hexy.trend.appsecurity.filter.MyLoginAuthenticationFilter;
import com.hexy.trend.appsecurity.handler.MyAuthenticationFailureHandler;
import com.hexy.trend.appsecurity.handler.MyAuthenticationSuccessHandler;
import com.hexy.trend.appsecurity.handler.MyLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyAuthenticationSuccessHandler successHandler;

    @Autowired
    private MyAuthenticationFailureHandler failureHandler;

    @Autowired
    private MyLogoutSuccessHandler logoutSuccessHandler;

    /** 配置认证处理逻辑 **/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                // 设置访问白名单（匹配这些url不需要认证）
                .antMatchers("/**/*.css").permitAll()
                .antMatchers("/**/*.js").permitAll()
                .antMatchers("/**/*.html").permitAll()
                .antMatchers("/**/api-docs").permitAll()
                .antMatchers("/**/swagger-resources").permitAll()

                // 设置访问白名单（生成加密密码接口）
                .antMatchers("/**/password").permitAll()
                .antMatchers("/**/register/**").permitAll()
                // 开发时方便放开所有权限,上线后要注释掉
                .antMatchers("/**").permitAll()

                // 其他的请求都需要进行认证
                .anyRequest().authenticated()

                // 取消页面登录
                .and()
                .formLogin()
                .loginProcessingUrl("/login")
                .successHandler(successHandler)
                .failureHandler(failureHandler)
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()

                // 登出设置
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(logoutSuccessHandler)
                .permitAll()

                // 关闭CSRF
                .and()
                .csrf().disable()

                // 自定义认证过滤区分登录方式
                .addFilterBefore(myLoginAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                // 认证过滤(JWT)
                .addFilter(new MyJwtAuthenticationFilter(super.authenticationManager()))

                // 进入点设置
                .exceptionHandling().authenticationEntryPoint(new MyAuthenticationEntryPoint())

                // 禁用session
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                // 禁用缓存
                .and()
                .headers().cacheControl();
    }

    @Bean
    public MyLoginAuthenticationFilter myLoginAuthenticationFilter() throws Exception {
        MyLoginAuthenticationFilter filter = new MyLoginAuthenticationFilter();
        filter.setAuthenticationFailureHandler(failureHandler);
        filter.setAuthenticationSuccessHandler(successHandler);
        filter.setAuthenticationManager(authenticationManagerBean());
        return filter;
    }

    @Bean
    public AuthenticationProvider authenticationProvider(){
        AuthenticationProvider provider = new TrendAuthenticationProvider() ;
        return provider;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }
}

