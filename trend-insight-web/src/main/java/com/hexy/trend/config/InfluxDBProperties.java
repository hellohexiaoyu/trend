package com.hexy.trend.config;
import com.influxdb.client.*;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.internal.AbstractWriteClient;
import com.influxdb.client.write.Point;
import com.influxdb.query.FluxTable;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
        import org.springframework.context.annotation.PropertySource;
        import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author skies
 */
@Repository
public class InfluxDBProperties {

    @Autowired
    InfluxDBClient influxDBClient;

    @Value("${spring.influx.org:''}")
    private String org;

    @Value("${spring.influx.bucket:''}")
    private String bucket;

    /**
     * 保存
     * @param measurement 表名
     * @param fields
     */
    public void save(String measurement, Map<String,Object> fields){



        Point point = Point
                .measurement("mem")
                .addTag("department", fields.get("department").toString())
                .addTag("searchTerm",fields.get("searchTerm").toString())
                .addTag("date",fields.get("date").toString())
                .addField("sort", fields.get("sort").toString())
                .time(Instant.now(), WritePrecision.NS);

        WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();
        writeApi.writePoint(bucket, org, point);
    }
    public void saveBatch(String measurement, List<Map<String,Object>> fields){

        List<Point> batchPoints = new ArrayList<>();
        for (Map<String,Object> dataPoint : fields) {
            Point point = Point
                    .measurement("mem")
                    .addTag("department", dataPoint.get("department").toString())
                    .addTag("searchTerm",dataPoint.get("searchTerm").toString())
                    .addTag("date",dataPoint.get("date").toString())
                    .addField("sort", dataPoint.get("sort").toString())
                    .time(Instant.now(), WritePrecision.NS);
            batchPoints.add(point);
        }
        WriteApiBlocking writeApi = influxDBClient.getWriteApiBlocking();
        writeApi.writePoints(bucket, org, batchPoints);
    }
    /**
     * 查询语法说明

     * https://blog.52itstyle.vip

     * 1、bucket 桶
     * 2、range 指定起始时间段
     *    range有两个参数start，stop，stop不设置默认为当前。
     *    range可以是相对的（使用负持续时间）或绝对（使用时间段）
     * 3、filter 过滤条件查询 _measurement 表  _field 字段
     * 4、yield()函数作为查询结果输出过滤的tables。
     * 更多参考：https://docs.influxdata.com/influxdb/v2.0/query-data/flux/
     * @return
     */
    public List<FluxTable> findAll(){
        String sql = "from(bucket: \"%s\") |> range(start: -1m)";
        sql +="  |> filter(fn: (r) => r._measurement == \"%s\" and";
        sql +="  r._field == \"%s\")";
        sql +="  |> yield()";
        String flux = String.format(sql, bucket,"mem","t_amazon_ca");
        QueryApi queryApi = influxDBClient.getQueryApi();
        List<FluxTable> tables = queryApi.query(flux,org);
        return tables;
    }
}
