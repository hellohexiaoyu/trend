//package com.hexy.trend.config;
//
//import javax.sql.DataSource;
//
//import org.springframework.boot.jdbc.DataSourceBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class DataSourceConfig {
//
//    @Bean
//    public DataSource dataSource() {
//        return DataSourceBuilder
//                .create()
//                .url("jdbc:mysql://127.0.0.1:3306/trend_insight_dev?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&serverTimezone=Asia/Shanghai")
//                .username("root")
//                .password("root")
//                .driverClassName("com.mysql.jdbc.Driver")
//                .build();
//    }
//}
//
