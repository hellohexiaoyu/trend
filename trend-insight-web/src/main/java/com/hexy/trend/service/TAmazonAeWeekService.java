package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAeWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAeWeekService extends IService<TAmazonAeWeek> {
    public List<TAmazonAeWeek> getAmazonAeList(Date date);
    public void addBatch(List<TAmazonAeWeek> list) ;

    public void updateBatch(List<TAmazonAeWeek> list);
    public Page<TAmazonAeWeek> getRankingList(Page<TAmazonAeWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonAeWeek> getRankingListTask(Page<TAmazonAeWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonAeWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
