package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonFr;
import com.hexy.trend.entity.TAmazonIt;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonItService extends IService<TAmazonIt> {
    public List<TAmazonIt> getAmazonItList(Date date);

    public void addBatch(List<TAmazonIt> list) ;

    public void updateBatch(List<TAmazonIt> list);
    public Page<TAmazonIt> getRankingList(Page<TAmazonIt> page, InsightRO.GetInsightRO ro);
    public List<TAmazonIt> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonIt> getRankingListTask(Page<TAmazonIt> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonIt> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
