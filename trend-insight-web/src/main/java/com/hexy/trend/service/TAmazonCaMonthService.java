package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonCaWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonCaMonthService extends IService<TAmazonCaMonth> {
    public List<TAmazonCaMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonCaMonth> list) ;

    public void updateBatch(List<TAmazonCaMonth> list);
    public Page<TAmazonCaMonth> getRankingList(Page<TAmazonCaMonth> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonCaMonth> getRankingListTask(Page<TAmazonCaMonth> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonCaMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
