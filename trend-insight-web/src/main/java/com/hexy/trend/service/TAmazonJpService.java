package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonIt;
import com.hexy.trend.entity.TAmazonJp;
import com.hexy.trend.entity.TAmazonMx;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonJpService extends IService<TAmazonJp> {
    public List<TAmazonJp> getAmazonJpList(Date date);

    public void addBatch(List<TAmazonJp> list) ;

    public void updateBatch(List<TAmazonJp> list);
    public Page<TAmazonJp> getRankingList(Page<TAmazonJp> page, InsightRO.GetInsightRO ro);
    public List<TAmazonJp> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonJp> getRankingListTask(Page<TAmazonJp> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonJp> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
