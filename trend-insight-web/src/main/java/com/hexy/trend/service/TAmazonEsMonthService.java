package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonDeWeek;
import com.hexy.trend.entity.TAmazonEsMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonEsMonthService extends IService<TAmazonEsMonth> {
    public List<TAmazonEsMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonEsMonth> list) ;

    public void updateBatch(List<TAmazonEsMonth> list);
    public Page<TAmazonEsMonth> getRankingList(Page<TAmazonEsMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonEsMonth> getRankingListTask(Page<TAmazonEsMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonEsMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
