package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.entity.TAmazonRecord;

/**
 * <p>
 * 更新记录表 服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-02
 */
public interface TAmazonRecordService extends IService<TAmazonRecord> {
    public Integer saveRecord(TAmazonRecord record);

    public TAmazonRecord getRecordByDepartment(String department,Integer type);
}
