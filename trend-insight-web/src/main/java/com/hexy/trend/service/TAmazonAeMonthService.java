package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAeMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAeMonthService extends IService<TAmazonAeMonth> {
    public List<TAmazonAeMonth> getAmazonAeList(Date date);
    public void addBatch(List<TAmazonAeMonth> list) ;

    public void updateBatch(List<TAmazonAeMonth> list);
    public Page<TAmazonAeMonth> getRankingList(Page<TAmazonAeMonth> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonAeMonth> getRankingListTask(Page<TAmazonAeMonth> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonAeMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
