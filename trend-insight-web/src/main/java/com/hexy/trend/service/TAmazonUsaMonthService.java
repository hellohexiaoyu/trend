package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonUkWeek;
import com.hexy.trend.entity.TAmazonUsaMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonUsaMonthService extends IService<TAmazonUsaMonth> {
    public List<TAmazonUsaMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonUsaMonth> list) ;

    public void updateBatch(List<TAmazonUsaMonth> list);
    public Page<TAmazonUsaMonth> getRankingList(Page<TAmazonUsaMonth> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonUsaMonth> getRankingListTask(Page<TAmazonUsaMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonUsaMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
