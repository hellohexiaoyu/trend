package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAu;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAuService extends IService<TAmazonAu> {
    public List<TAmazonAu> getAmazonAuList(Date date);

    public void addBatch(List<TAmazonAu> list) ;

    public void updateBatch(List<TAmazonAu> list);
    public Page<TAmazonAu> getRankingList(Page<TAmazonAu> page, InsightRO.GetInsightRO ro);

    public List<TAmazonAu> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonAu> getRankingListTask(Page<TAmazonAu> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonAu> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
