package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAuWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAuWeekService extends IService<TAmazonAuWeek> {
    public List<TAmazonAuWeek> getAmazonAuList(Date date);
    public void addBatch(List<TAmazonAuWeek> list) ;

    public void updateBatch(List<TAmazonAuWeek> list);
    public Page<TAmazonAuWeek> getRankingList(Page<TAmazonAuWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonAuWeek> getRankingListTask(Page<TAmazonAuWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonAuWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
