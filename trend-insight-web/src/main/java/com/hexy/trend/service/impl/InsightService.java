package com.hexy.trend.service.impl;

import com.alibaba.fastjson.JSON;
import com.hexy.trend.dao.*;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaWeek;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.redis.util.RedisUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;


@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class InsightService {
    @Resource
    private TAmazonCaMapper caMapper;

    @Resource
    private TAmazonDeMapper deMapper;
    @Resource
    private TAmazonEsMapper esMapper;

    @Resource
    private TAmazonFrMapper frMapper;

    @Resource
    private TAmazonItMapper itMapper;

    @Resource
    private TAmazonJpMapper jpMapper;
    @Resource
    private TAmazonMxMapper mxMapper;
    @Resource
    private TAmazonUkMapper ukMapper;
    @Resource
    private TAmazonUsaMapper usaMapper;
    @Resource
    private TAmazonAeMapper aeMapper;
    @Resource
    private TAmazonAuMapper auMapper;
    @Resource
    private TAmazonInMapper inMapper;

    //周

    @Resource
    private TAmazonCaWeekMapper caWeekMapper;

    @Resource
    private TAmazonDeWeekMapper deWeekMapper;
    @Resource
    private TAmazonEsWeekMapper esWeekMapper;

    @Resource
    private TAmazonFrWeekMapper frWeekMapper;

    @Resource
    private TAmazonItWeekMapper itWeekMapper;

    @Resource
    private TAmazonJpWeekMapper jpWeekMapper;
    @Resource
    private TAmazonMxWeekMapper mxWeekMapper;
    @Resource
    private TAmazonUkWeekMapper ukWeekMapper;
    @Resource
    private TAmazonUsaWeekMapper usaWeekMapper;
    @Resource
    private TAmazonAeWeekMapper aeWeekMapper;
    @Resource
    private TAmazonAuWeekMapper auWeekMapper;
    @Resource
    private TAmazonInWeekMapper inWeekMapper;


    //月
    @Resource
    private TAmazonCaMonthMapper caMonthMapper;

    @Resource
    private TAmazonDeMonthMapper deMonthMapper;
    @Resource
    private TAmazonEsMonthMapper esMonthMapper;

    @Resource
    private TAmazonFrMonthMapper frMonthMapper;

    @Resource
    private TAmazonItMonthMapper itMonthMapper;

    @Resource
    private TAmazonJpMonthMapper jpMonthMapper;
    @Resource
    private TAmazonMxMonthMapper mxMonthMapper;
    @Resource
    private TAmazonUkMonthMapper ukMonthMapper;
    @Resource
    private TAmazonUsaMonthMapper usaMonthMapper;
    @Resource
    private TAmazonAeMonthMapper aeMonthMapper;
    @Resource
    private TAmazonAuMonthMapper auMonthMapper;
    @Resource
    private TAmazonInMonthMapper inMonthMapper;
    @Autowired
    private RedisUtil redisUtil;
    public void clearData(){
        caMapper.createTempTableToDelete();
        caMapper.deleteRecordsBasedOnTempTable();
        caMapper.dropTempTable();

        deMapper.createTempTableToDelete();
        deMapper.deleteRecordsBasedOnTempTable();
        deMapper.dropTempTable();

        esMapper.createTempTableToDelete();
        esMapper.deleteRecordsBasedOnTempTable();
        esMapper.dropTempTable();

        frMapper.createTempTableToDelete();
        frMapper.deleteRecordsBasedOnTempTable();
        frMapper.dropTempTable();

        itMapper.createTempTableToDelete();
        itMapper.deleteRecordsBasedOnTempTable();
        itMapper.dropTempTable();

        jpMapper.createTempTableToDelete();
        jpMapper.deleteRecordsBasedOnTempTable();
        jpMapper.dropTempTable();

        mxMapper.createTempTableToDelete();
        mxMapper.deleteRecordsBasedOnTempTable();
        mxMapper.dropTempTable();

        ukMapper.createTempTableToDelete();
        ukMapper.deleteRecordsBasedOnTempTable();
        ukMapper.dropTempTable();

        usaMapper.createTempTableToDelete();
        usaMapper.deleteRecordsBasedOnTempTable();
        usaMapper.dropTempTable();

        aeMapper.createTempTableToDelete();
        aeMapper.deleteRecordsBasedOnTempTable();
        aeMapper.dropTempTable();

        auMapper.createTempTableToDelete();
        auMapper.deleteRecordsBasedOnTempTable();
        auMapper.dropTempTable();


        inMapper.createTempTableToDelete();
        inMapper.deleteRecordsBasedOnTempTable();
        inMapper.dropTempTable();
    }

    public void clearDataWeek(){
        caWeekMapper.createTempTableToDelete();
        caWeekMapper.deleteRecordsBasedOnTempTable();
        caWeekMapper.dropTempTable();

        deWeekMapper.createTempTableToDelete();
        deWeekMapper.deleteRecordsBasedOnTempTable();
        deWeekMapper.dropTempTable();

        esWeekMapper.createTempTableToDelete();
        esWeekMapper.deleteRecordsBasedOnTempTable();
        esWeekMapper.dropTempTable();

        frWeekMapper.createTempTableToDelete();
        frWeekMapper.deleteRecordsBasedOnTempTable();
        frWeekMapper.dropTempTable();

        itWeekMapper.createTempTableToDelete();
        itWeekMapper.deleteRecordsBasedOnTempTable();
        itWeekMapper.dropTempTable();

        jpWeekMapper.createTempTableToDelete();
        jpWeekMapper.deleteRecordsBasedOnTempTable();
        jpWeekMapper.dropTempTable();

        mxWeekMapper.createTempTableToDelete();
        mxWeekMapper.deleteRecordsBasedOnTempTable();
        mxWeekMapper.dropTempTable();

        ukWeekMapper.createTempTableToDelete();
        ukWeekMapper.deleteRecordsBasedOnTempTable();
        ukWeekMapper.dropTempTable();

        usaWeekMapper.createTempTableToDelete();
        usaWeekMapper.deleteRecordsBasedOnTempTable();
        usaWeekMapper.dropTempTable();

        aeWeekMapper.createTempTableToDelete();
        aeWeekMapper.deleteRecordsBasedOnTempTable();
        aeWeekMapper.dropTempTable();

        auWeekMapper.createTempTableToDelete();
        auWeekMapper.deleteRecordsBasedOnTempTable();
        auWeekMapper.dropTempTable();

        inWeekMapper.createTempTableToDelete();
        inWeekMapper.deleteRecordsBasedOnTempTable();
        inWeekMapper.dropTempTable();
    }

    public void clearDataMonth(){
        caMonthMapper.createTempTableToDelete();
        caMonthMapper.deleteRecordsBasedOnTempTable();
        caMonthMapper.dropTempTable();

        deMonthMapper.createTempTableToDelete();
        deMonthMapper.deleteRecordsBasedOnTempTable();
        deMonthMapper.dropTempTable();

        esMonthMapper.createTempTableToDelete();
        esMonthMapper.deleteRecordsBasedOnTempTable();
        esMonthMapper.dropTempTable();

        frMonthMapper.createTempTableToDelete();
        frMonthMapper.deleteRecordsBasedOnTempTable();
        frMonthMapper.dropTempTable();

        itMonthMapper.createTempTableToDelete();
        itMonthMapper.deleteRecordsBasedOnTempTable();
        itMonthMapper.dropTempTable();

        jpMonthMapper.createTempTableToDelete();
        jpMonthMapper.deleteRecordsBasedOnTempTable();
        jpMonthMapper.dropTempTable();

        mxMonthMapper.createTempTableToDelete();
        mxMonthMapper.deleteRecordsBasedOnTempTable();
        mxMonthMapper.dropTempTable();

        ukMonthMapper.createTempTableToDelete();
        ukMonthMapper.deleteRecordsBasedOnTempTable();
        ukMonthMapper.dropTempTable();

        usaMonthMapper.createTempTableToDelete();
        usaMonthMapper.deleteRecordsBasedOnTempTable();
        usaMonthMapper.dropTempTable();

        aeMonthMapper.createTempTableToDelete();
        aeMonthMapper.deleteRecordsBasedOnTempTable();
        aeMonthMapper.dropTempTable();

        auMonthMapper.createTempTableToDelete();
        auMonthMapper.deleteRecordsBasedOnTempTable();
        auMonthMapper.dropTempTable();

        inMonthMapper.createTempTableToDelete();
        inMonthMapper.deleteRecordsBasedOnTempTable();
        inMonthMapper.dropTempTable();

    }

    public <T> List<T> getSortList(String searchTerm,String departments, Class<T> returnType){

//        String departments =  DepartmentTypeEnum.getDescByValue(department);
        String redisKey = "trend:"+ departments +":" + searchTerm;

        String s = redisUtil.get(redisKey);
        if(null != s){
            List<T> tAmazonCas = JSON.parseArray(s, returnType);
            return tAmazonCas;
        }else {
            return null;
        }
    }
    public void delData(Date cutoffDate){
        caMapper.delData(cutoffDate);
        deMapper.delData(cutoffDate);
        esMapper.delData(cutoffDate);
        frMapper.delData(cutoffDate);
        itMapper.delData(cutoffDate);
        jpMapper.delData(cutoffDate);
        mxMapper.delData(cutoffDate);
        ukMapper.delData(cutoffDate);
        usaMapper.delData(cutoffDate);
        aeMapper.delData(cutoffDate);
        auMapper.delData(cutoffDate);
        inMapper.delData(cutoffDate);
    }
}
