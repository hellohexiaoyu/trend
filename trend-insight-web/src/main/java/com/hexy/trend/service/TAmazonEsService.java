package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonDe;
import com.hexy.trend.entity.TAmazonEs;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonEsService extends IService<TAmazonEs> {
    public List<TAmazonEs> getAmazonEsList(Date date);

    public void addBatch(List<TAmazonEs> list) ;

    public void updateBatch(List<TAmazonEs> list);
    public Page<TAmazonEs> getRankingList(Page<TAmazonEs> page, InsightRO.GetInsightRO ro);
    public List<TAmazonEs> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonEs> getRankingListTask(Page<TAmazonEs> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonEs> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
