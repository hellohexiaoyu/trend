package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonJp;
import com.hexy.trend.entity.TAmazonMx;
import io.swagger.models.auth.In;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonMxService extends IService<TAmazonMx> {
    public List<TAmazonMx> getAmazonMxList(Date date);

    public void addBatch(List<TAmazonMx> list) ;

    public void updateBatch(List<TAmazonMx> list);
    public Page<TAmazonMx> getRankingList(Page<TAmazonMx> page,InsightRO.GetInsightRO ro);
    public List<TAmazonMx> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonMx> getRankingListTask(Page<TAmazonMx> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonMx> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
