package com.hexy.trend.service;

import com.alibaba.excel.event.AnalysisEventListener;
import org.apache.poi.ss.formula.functions.T;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.List;
import java.util.Set;

public interface IEasyExcelService {
    public void readExcel(File file, Class<?> clazz, AnalysisEventListener<T> listenerHandler) throws Exception;

    public void exportExcel(HttpServletResponse response, String fileName, Class<?> dataClass, List<?> dataList, Set<String> excludeColumn)throws Exception;
}
