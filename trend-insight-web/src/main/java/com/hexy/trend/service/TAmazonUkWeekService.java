package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonUkMonth;
import com.hexy.trend.entity.TAmazonUkWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonUkWeekService extends IService<TAmazonUkWeek> {
    public List<TAmazonUkWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonUkWeek> list) ;

    public void updateBatch(List<TAmazonUkWeek> list);
    public Page<TAmazonUkWeek> getRankingList(Page<TAmazonUkWeek> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonUkWeek> getRankingListTask(Page<TAmazonUkWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonUkWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
