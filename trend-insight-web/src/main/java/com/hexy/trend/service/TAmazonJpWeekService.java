package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonJpMonth;
import com.hexy.trend.entity.TAmazonJpWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonJpWeekService extends IService<TAmazonJpWeek> {
    public List<TAmazonJpWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonJpWeek> list) ;

    public void updateBatch(List<TAmazonJpWeek> list);
    public Page<TAmazonJpWeek> getRankingList(Page<TAmazonJpWeek> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonJpWeek> getRankingListTask(Page<TAmazonJpWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonJpWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
