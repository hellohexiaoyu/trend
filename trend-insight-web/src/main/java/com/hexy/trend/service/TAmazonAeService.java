package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAe;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAeService extends IService<TAmazonAe> {
    public List<TAmazonAe> getAmazonAeList(Date date);

    public void addBatch(List<TAmazonAe> list) ;

    public void updateBatch(List<TAmazonAe> list);
    public List<TAmazonAe> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
    public Page<TAmazonAe> getRankingList(Page<TAmazonAe> page, InsightRO.GetInsightRO ro);
    public Page<TAmazonAe> getRankingListTask(Page<TAmazonAe> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);
}
