package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonItMonth;
import com.hexy.trend.entity.TAmazonItWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonItWeekService extends IService<TAmazonItWeek> {
    public List<TAmazonItWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonItWeek> list) ;

    public void updateBatch(List<TAmazonItWeek> list);
    public Page<TAmazonItWeek> getRankingList(Page<TAmazonItWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonItWeek> getRankingListTask(Page<TAmazonItWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonItWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
