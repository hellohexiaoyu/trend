package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonInWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonInWeekService extends IService<TAmazonInWeek> {
    public List<TAmazonInWeek> getAmazonInList(Date date);
    public void addBatch(List<TAmazonInWeek> list) ;

    public void updateBatch(List<TAmazonInWeek> list);
    public Page<TAmazonInWeek> getRankingList(Page<TAmazonInWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonInWeek> getRankingListTask(Page<TAmazonInWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonInWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
