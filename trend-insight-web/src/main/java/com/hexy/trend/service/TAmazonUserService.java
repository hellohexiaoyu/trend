package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonUser;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
public interface TAmazonUserService extends IService<TAmazonUser> {
   TAmazonUser getUserByUsername(String username);
   Integer saveUser(TAmazonUser user);

   List<TAmazonUser> getUserByType(Integer type);

   Integer setUser(TAmazonUser user);

   TAmazonUser getUserById(Integer id);

   Page<TAmazonUser> getUserList(Page<TAmazonUser> page, String userName, Integer type, Date startTime, Date endTime);
}
