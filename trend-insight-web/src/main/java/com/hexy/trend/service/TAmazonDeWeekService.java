package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonDeMonth;
import com.hexy.trend.entity.TAmazonDeWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonDeWeekService extends IService<TAmazonDeWeek> {
    public List<TAmazonDeWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonDeWeek> list) ;

    public void updateBatch(List<TAmazonDeWeek> list);
    public Page<TAmazonDeWeek> getRankingList(Page<TAmazonDeWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonDeWeek> getRankingListTask(Page<TAmazonDeWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonDeWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
