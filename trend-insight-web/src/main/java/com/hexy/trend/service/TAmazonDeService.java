package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonDe;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonDeService extends IService<TAmazonDe> {
    public List<TAmazonDe> getAmazonDeList(Date date);

    public void addBatch(List<TAmazonDe> list) ;

    public void updateBatch(List<TAmazonDe> list);
    public Page<TAmazonDe> getRankingList(Page<TAmazonDe> page, InsightRO.GetInsightRO ro);

    public List<TAmazonDe> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonDe> getRankingListTask(Page<TAmazonDe> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonDe> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
