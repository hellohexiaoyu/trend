package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonFrMonth;
import com.hexy.trend.entity.TAmazonFrWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonFrWeekService extends IService<TAmazonFrWeek> {
    public List<TAmazonFrWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonFrWeek> list) ;

    public void updateBatch(List<TAmazonFrWeek> list);
    public Page<TAmazonFrWeek> getRankingList(Page<TAmazonFrWeek> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonFrWeek> getRankingListTask(Page<TAmazonFrWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonFrWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
