package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonMxMonth;
import com.hexy.trend.entity.TAmazonMxWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonMxWeekService extends IService<TAmazonMxWeek> {
    public List<TAmazonMxWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonMxWeek> list) ;

    public void updateBatch(List<TAmazonMxWeek> list);
    public Page<TAmazonMxWeek> getRankingList(Page<TAmazonMxWeek> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonMxWeek> getRankingListTask(Page<TAmazonMxWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonMxWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
