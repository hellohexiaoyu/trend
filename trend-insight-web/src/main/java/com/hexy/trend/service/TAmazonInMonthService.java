package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonInMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonInMonthService extends IService<TAmazonInMonth> {
    public List<TAmazonInMonth> getAmazonInList(Date date);
    public void addBatch(List<TAmazonInMonth> list) ;

    public void updateBatch(List<TAmazonInMonth> list);
    public Page<TAmazonInMonth> getRankingList(Page<TAmazonInMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonInMonth> getRankingListTask(Page<TAmazonInMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonInMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
