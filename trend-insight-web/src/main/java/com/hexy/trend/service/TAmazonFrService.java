package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonEs;
import com.hexy.trend.entity.TAmazonFr;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonFrService extends IService<TAmazonFr> {
    public List<TAmazonFr> getAmazonFrList(Date date);

    public void addBatch(List<TAmazonFr> list) ;

    public void updateBatch(List<TAmazonFr> list);
    public Page<TAmazonFr> getRankingList(Page<TAmazonFr> page, InsightRO.GetInsightRO ro);
    public List<TAmazonFr> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonFr> getRankingListTask(Page<TAmazonFr> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonFr> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
