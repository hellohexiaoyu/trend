package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonUk;
import com.hexy.trend.entity.TAmazonUsa;
import com.hexy.trend.entity.TAmazonUsa;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-13
 */
public interface TAmazonUsaService extends IService<TAmazonUsa> {
    public List<TAmazonUsa> getAmazonUsaList(Date date);

    public void addBatch(List<TAmazonUsa> list) ;

    public void updateBatch(List<TAmazonUsa> list);
    public Page<TAmazonUsa> getRankingList(Page<TAmazonUsa> page, InsightRO.GetInsightRO ro);

    public List<TAmazonUsa> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonUsa> getRankingListTask(Page<TAmazonUsa> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonUsa> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
