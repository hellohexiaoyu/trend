package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonItWeek;
import com.hexy.trend.entity.TAmazonJpMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonJpMonthService extends IService<TAmazonJpMonth> {
    public List<TAmazonJpMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonJpMonth> list) ;

    public void updateBatch(List<TAmazonJpMonth> list);
    public Page<TAmazonJpMonth> getRankingList(Page<TAmazonJpMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonJpMonth> getRankingListTask(Page<TAmazonJpMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonJpMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
