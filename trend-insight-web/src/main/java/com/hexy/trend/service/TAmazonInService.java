package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonIn;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonInService extends IService<TAmazonIn> {
    public List<TAmazonIn> getAmazonInList(Date date);

    public void addBatch(List<TAmazonIn> list) ;

    public void updateBatch(List<TAmazonIn> list);
    public Page<TAmazonIn> getRankingList(Page<TAmazonIn> page, InsightRO.GetInsightRO ro);
    public List<TAmazonIn> getRankingList(InsightRO.GetInsightDepartmentRO ro);

    public Page<TAmazonIn> getRankingListTask(Page<TAmazonIn> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonIn> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
