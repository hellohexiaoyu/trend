package com.hexy.trend.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hexy.trend.dao.TAmazonUserMapper;
import com.hexy.trend.entity.TAmazonUsa;
import com.hexy.trend.entity.TAmazonUser;
import com.hexy.trend.enums.AcountTypeEnum;
import com.hexy.trend.mybatis.converter.ExcludeEmptyQueryWrapper;
import com.hexy.trend.service.TAmazonUserService;
import com.hexy.trend.web.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author hexy
 * @since 2023-10-25
 */
@Service
public class TAmazonUserServiceImpl extends ServiceImpl<TAmazonUserMapper, TAmazonUser> implements TAmazonUserService {

    @Autowired
    private TAmazonUserMapper mapper;
    @Override
    public TAmazonUser getUserByUsername(String username) {
        ExcludeEmptyQueryWrapper<TAmazonUser> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("username",username).or().eq("email",username);
        return mapper.selectOne(qqw);
    }

    @Override
    public Integer saveUser(TAmazonUser user) {
        TAmazonUser byUsername = getUserByUsername(user.getUsername());
        if(null != byUsername){
            throw new BusinessException("当前账号已注册");
        }
        //新注册用户赠送一天vip
        // 获取当前时间
        Calendar calendar = Calendar.getInstance();
        Date startTime = calendar.getTime();
        // 加一天
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date endTime = calendar.getTime();
        user.setStartTime(startTime);
        user.setEndTime(endTime);
        user.setType(AcountTypeEnum.VIP.getValue());
        user.setEmail(user.getUsername());
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        return mapper.insert(user);
    }

    @Override
    public List<TAmazonUser> getUserByType(Integer type) {
        ExcludeEmptyQueryWrapper<TAmazonUser> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("type",type);
        return mapper.selectList(qqw);
    }

    @Override
    public Integer setUser(TAmazonUser user) {
        return mapper.updateById(user);
    }

    @Override
    public TAmazonUser getUserById(Integer id) {
        return mapper.selectById(id);
    }

    @Override
    public Page<TAmazonUser> getUserList(Page<TAmazonUser> page, String userName, Integer type, Date startTime, Date endTime) {

        ExcludeEmptyQueryWrapper<TAmazonUser> qqw = new ExcludeEmptyQueryWrapper();
        if(null != userName && !userName.equals("")){
            qqw.like("username",userName);
        }
        if(null != type){
            qqw.eq("type",type);
        }else{
            qqw.ne("type",AcountTypeEnum.ADMIN.getValue());
        }
        if(null != startTime){
            qqw.gt("start_time",startTime);
        }

        if(null != endTime){
            qqw.lt("end_time",endTime);
        }
        return mapper.selectPage(page,qqw);
    }
}
