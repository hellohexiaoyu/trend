package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonJpWeek;
import com.hexy.trend.entity.TAmazonMxMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonMxMonthService extends IService<TAmazonMxMonth> {
    public List<TAmazonMxMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonMxMonth> list) ;

    public void updateBatch(List<TAmazonMxMonth> list);
    public Page<TAmazonMxMonth> getRankingList(Page<TAmazonMxMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonMxMonth> getRankingListTask(Page<TAmazonMxMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonMxMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
