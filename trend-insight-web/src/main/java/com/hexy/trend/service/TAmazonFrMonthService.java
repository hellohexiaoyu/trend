package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonEsWeek;
import com.hexy.trend.entity.TAmazonFrMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonFrMonthService extends IService<TAmazonFrMonth> {
    public List<TAmazonFrMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonFrMonth> list) ;

    public void updateBatch(List<TAmazonFrMonth> list);
    public Page<TAmazonFrMonth> getRankingList(Page<TAmazonFrMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonFrMonth> getRankingListTask(Page<TAmazonFrMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonFrMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
