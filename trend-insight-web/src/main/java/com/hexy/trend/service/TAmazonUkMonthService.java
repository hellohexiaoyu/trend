package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonMxWeek;
import com.hexy.trend.entity.TAmazonUkMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonUkMonthService extends IService<TAmazonUkMonth> {
    public List<TAmazonUkMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonUkMonth> list) ;

    public void updateBatch(List<TAmazonUkMonth> list);
    public Page<TAmazonUkMonth> getRankingList(Page<TAmazonUkMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonUkMonth> getRankingListTask(Page<TAmazonUkMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonUkMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
