package com.hexy.trend.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.dao.TAmazonMxMapper;
import com.hexy.trend.dao.TAmazonMxMapper;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonJp;
import com.hexy.trend.entity.TAmazonMx;
import com.hexy.trend.entity.TAmazonMx;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.mybatis.converter.ExcludeEmptyQueryWrapper;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.TAmazonMxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
@Service
public class TAmazonMxServiceImpl extends ServiceImpl<TAmazonMxMapper, TAmazonMx> implements TAmazonMxService {
    @Autowired
    private TAmazonMxMapper mapper;

    @Autowired
    private RedisUtil redisUtil;
    @Override
    public List<TAmazonMx> getAmazonMxList(Date date) {
        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("date",date);
        List<TAmazonMx> ProductRankingThree = super.list(qqw);
        return ProductRankingThree;
    }

    @Async
    @Override
    public void addBatch(List<TAmazonMx> list) {
        mapper.insertBatchSomeColumn(list);
    }
    @Async
    @Override
    public void updateBatch(List<TAmazonMx> list) {
        mapper.updateBatch(list);
    }

    @Override
    public Page<TAmazonMx> getRankingList(Page<TAmazonMx> page,InsightRO.GetInsightRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf2.format(ro.getDate());

        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("date",format);
        if(null != ro.getSearchTerm() && !ro.getSearchTerm().isEmpty()){
            qqw.and(i -> i.like("search_term",ro.getSearchTerm())
                    .or().like("translate",ro.getSearchTerm()));
        }
        qqw.gt("day_rank",ro.getRankingDayMin());
        qqw.lt("day_rank",ro.getRankingDayMax());
        qqw.gt("yesterday_rank",ro.getRankingYesterdayMin());
        qqw.lt("yesterday_rank",ro.getRankingYesterdayMax());
        qqw.gt("previous_rank",ro.getRankingPreviousDayMin());
        qqw.lt("previous_rank",ro.getRankingPreviousDayMax());

        if(null != ro.getFloatDaySort() && ro.getFloatDaySort() == 2){
            if(null != ro.getFloatDayMin()){
                qqw.gt("day_float",ro.getFloatDayMin());
            }
            if(null != ro.getFloatDayMax()){
                qqw.lt("day_float",ro.getFloatDayMax());
            }
            if(null != ro.getFloatDayMax() || null != ro.getFloatDayMin()){
                //大于0
                qqw.gt("day_float",0);
            }
        }else{
            if(null != ro.getFloatDayMax()){
                qqw.gt("day_float",-ro.getFloatDayMax());
            }
            if(null != ro.getFloatDayMin()){
                qqw.lt("day_float",-ro.getFloatDayMin());
            }
            //小于0
            if(null != ro.getFloatDayMax() || null != ro.getFloatDayMin()){
                qqw.lt("day_float",0);
            }
        }
        if(null != ro.getFloatYesterdaySort() && ro.getFloatYesterdaySort() == 2){
            if(null != ro.getFloatYesterdayMin()){
                qqw.gt("yesterday_float",ro.getFloatYesterdayMin());
            }
            if(null != ro.getFloatYesterdayMax()){
                qqw.lt("yesterday_float",ro.getFloatYesterdayMax());
            }
            //大于0
            if(null != ro.getFloatYesterdayMax() || null != ro.getFloatYesterdayMin()){
                qqw.gt("yesterday_float",0);
            }
        }else{
            if(null != ro.getFloatYesterdayMax()){
                qqw.gt("yesterday_float",-ro.getFloatYesterdayMax());
            }
            if(null != ro.getFloatYesterdayMin()){
                qqw.lt("yesterday_float",-ro.getFloatYesterdayMin());
            }
            //小于0
            if(null != ro.getFloatYesterdayMax() || null != ro.getFloatYesterdayMin()){
                qqw.lt("yesterday_float",0);
            }
        }
        if(null != ro.getFloatPreviousDaySort() && ro.getFloatPreviousDaySort() == 2){
            if(null != ro.getFloatPreviousDayMin()){
                qqw.gt("previous_float",ro.getFloatPreviousDayMin());
            }
            if(null != ro.getFloatPreviousDayMax()){
                qqw.lt("previous_float",ro.getFloatPreviousDayMax());
            }
            //大于0
            if(null != ro.getFloatPreviousDayMax() || null != ro.getFloatPreviousDayMin()){
                qqw.gt("previous_float",0);
            }
        }else{
            if(null != ro.getFloatPreviousDayMax()){
                qqw.gt("previous_float",-ro.getFloatPreviousDayMax());
            }
            if(null != ro.getFloatPreviousDayMin()){
                qqw.lt("previous_float",-ro.getFloatPreviousDayMin());
            }
            //小于0
            if(null != ro.getFloatPreviousDayMax() || null != ro.getFloatPreviousDayMin()){
                qqw.lt("previous_float",0);
            }
        }

        if(null != ro.getRankingDaySort() && ro.getRankingDaySort() == 2){
            qqw.orderByDesc("day_rank");
        }else{
            qqw.orderByAsc("day_rank");
        }

        Page<TAmazonMx> three = mapper.selectPage( page, qqw);
//        List<TAmazonMx> records = three.getRecords();
//
//        for(TAmazonMx record : records){
//            String department =  DepartmentTypeEnum.getDescByValue(ro.getDepartment());
//            String redisKey = "trend:"+ department +":" + record.getSearchTerm();
//            String s = redisUtil.get(redisKey);
//            if(null != s){
//                List<TAmazonMx> tAmazonMxs = JSON.parseArray(s, TAmazonMx.class);
//                record.setSort(tAmazonMxs);
//            }
//        }
//        three.setRecords(records);
        return three;
    }

    @Override
    public List<TAmazonMx> getRankingList(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());

        List<TAmazonMx> TAmazonMxs = mapper.selectList(qqw);
        return TAmazonMxs;
    }

    @Override
    public Page<TAmazonMx> getRankingListTask(Page<TAmazonMx> page, InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());
        qqw.orderByAsc("date");
        Page<TAmazonMx> three = mapper.selectPage(page, qqw);
        return three;
    }

    @Override
    public Integer getCount(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        Integer integer = mapper.selectCount(qqw);
        return integer;
    }

    @Override
    public List<TAmazonMx> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonMx> qqw = new ExcludeEmptyQueryWrapper();
//        qqw.ge("date",s);
//        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());
        qqw.orderByAsc("date");
        List<TAmazonMx> three = mapper.selectList(qqw);
        return three;
    }
}
