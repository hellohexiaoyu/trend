package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonAuMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2024-01-26
 */
public interface TAmazonAuMonthService extends IService<TAmazonAuMonth> {
    public List<TAmazonAuMonth> getAmazonAuList(Date date);
    public void addBatch(List<TAmazonAuMonth> list) ;

    public void updateBatch(List<TAmazonAuMonth> list);
    public Page<TAmazonAuMonth> getRankingList(Page<TAmazonAuMonth> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonAuMonth> getRankingListTask(Page<TAmazonAuMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);



    public List<TAmazonAuMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
