package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonMx;
import com.hexy.trend.entity.TAmazonUk;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-16
 */
public interface TAmazonUkService extends IService<TAmazonUk> {
    public List<TAmazonUk> getAmazonUkList(Date date);

    public void addBatch(List<TAmazonUk> list) ;

    public void updateBatch(List<TAmazonUk> list);
    public Page<TAmazonUk> getRankingList(Page<TAmazonUk> page, InsightRO.GetInsightRO ro);
    public List<TAmazonUk> getRankingList(InsightRO.GetInsightDepartmentRO ro);
    public Page<TAmazonUk> getRankingListTask(Page<TAmazonUk> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);

    public List<TAmazonUk> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
