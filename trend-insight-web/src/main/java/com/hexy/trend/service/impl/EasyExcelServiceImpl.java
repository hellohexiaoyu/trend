package com.hexy.trend.service.impl;


import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.metadata.style.WriteFont;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import com.hexy.trend.bean.ExcelParams;
import com.hexy.trend.handler.AutoNoHandler;
import com.hexy.trend.service.IEasyExcelService;
import com.hexy.trend.util.EasyExcelUtils;
import com.hexy.trend.web.exception.BusinessException;
import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Set;

@Service
@Transactional(rollbackFor=Exception.class)
public class EasyExcelServiceImpl implements IEasyExcelService {
    @Override
    public void readExcel(File file, Class<?> clazz, AnalysisEventListener<T> listenerHandler) throws Exception {

        InputStream is = null;
        try {
            is = new FileInputStream(file);
            // 封装Excel参数
            ExcelParams<T> params = new ExcelParams<>(is, clazz);

            params.setListener(listenerHandler);
            // 读取Excel文件
            EasyExcelUtils.readExcel(params);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }finally {
            is.close();
        }

    }

    @Override
    public void exportExcel(HttpServletResponse response,String fileName, Class<?> dataClass, List<?> dataList,Set<String> excludeColumn) {

        WriteCellStyle headWriteCellStyle = new WriteCellStyle();
        // 设置表头背景色为灰色
        headWriteCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        WriteFont headWriteFont = new WriteFont();
        headWriteFont.setFontHeightInPoints((short) 12);
        headWriteCellStyle.setWriteFont(headWriteFont);
        // 设置表头水平垂直居中
        headWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
        // 设置表头边框样式
        headWriteCellStyle.setBorderLeft(BorderStyle.THIN);
        headWriteCellStyle.setBorderTop(BorderStyle.THIN);
        headWriteCellStyle.setBorderRight(BorderStyle.THIN);
        headWriteCellStyle.setBorderBottom(BorderStyle.THIN);

        // 设置内容样式
        WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
        WriteFont contentWriteFont = new WriteFont();
        contentWriteFont.setFontHeightInPoints((short) 12);
        contentWriteCellStyle.setWriteFont(contentWriteFont);
        contentWriteCellStyle.setWrapped(true);
        contentWriteCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);

        // 创建水平样式策略
        HorizontalCellStyleStrategy horizontalCellStyleStrategy =
                new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);

        try {
            this.setExcelResponseProp(response, fileName);
            EasyExcel.write(response.getOutputStream())
                    .head(dataClass)
                    .registerWriteHandler(horizontalCellStyleStrategy)
                    .registerWriteHandler(new AutoNoHandler())
                    .excludeColumnFieldNames(excludeColumn)
                    .excelType(ExcelTypeEnum.XLSX)
                    .sheet("sheet1")
                    .doWrite(dataList);

        } catch (Exception e) {
            throw new BusinessException(e);
        }


    }


    /**
     * 设置响应结果
     *
     * @param response    响应结果对象
     * @param rawFileName 文件名
     * @throws UnsupportedEncodingException 不支持编码异常
     */
    private void setExcelResponseProp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(rawFileName, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
    }

    public static void main(String[] args) throws Exception {

        String fileName = "F:\\cfcode2023\\chem.xls";
        File file = new File(fileName);
        EasyExcelServiceImpl ss = new EasyExcelServiceImpl();

//        ss.readExcel(file,MdtCheckupChemExcel.class,new MdtChemExcelListenerHandler<>(schema));
//        File file = new File(fileName);
//        InputStream is = new FileInputStream(file);
//
//        // 封装Excel参数
//        ExcelParams<MdtCheckupChemExcel> params = new ExcelParams<>(is, MdtCheckupChemExcel.class);
//
//        params.setListener(new MdtChemExcelListenerHandler<MdtCheckupChemExcel>());
//        // 读取Excel文件
//        EasyExcelUtils.readExcel(params);
    }
}
