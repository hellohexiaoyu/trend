package com.hexy.trend.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.dao.TAmazonUsaMapper;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonUk;
import com.hexy.trend.entity.TAmazonUsa;
import com.hexy.trend.enums.DepartmentTypeEnum;
import com.hexy.trend.mybatis.converter.ExcludeEmptyQueryWrapper;
import com.hexy.trend.redis.util.RedisUtil;
import com.hexy.trend.service.TAmazonUsaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hexy
 * @since 2023-10-13
 */
@Service
public class TAmazonUsaServiceImpl extends ServiceImpl<TAmazonUsaMapper, TAmazonUsa> implements TAmazonUsaService {
    @Autowired
    private TAmazonUsaMapper mapper;

    @Autowired
    private RedisUtil redisUtil;
    @Override
    public List<TAmazonUsa> getAmazonUsaList(Date date) {
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("date",date);
        List<TAmazonUsa> ProductRankingThree = super.list(qqw);
        return ProductRankingThree;
    }

    @Async
    @Override
    public void addBatch(List<TAmazonUsa> list) {
        mapper.insertBatchSomeColumn(list);
    }
    @Async
    @Override
    public void updateBatch(List<TAmazonUsa> list) {
        mapper.updateBatch(list);
    }

    @Override
    public Page<TAmazonUsa> getRankingList(Page<TAmazonUsa> page, InsightRO.GetInsightRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf2.format(ro.getDate());
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("date",format);
        if(null != ro.getSearchTerm() && !ro.getSearchTerm().isEmpty()){
            qqw.and(i -> i.like("search_term",ro.getSearchTerm())
                    .or().like("translate",ro.getSearchTerm()));
        }
        qqw.gt("day_rank",ro.getRankingDayMin());
        qqw.lt("day_rank",ro.getRankingDayMax());
        qqw.gt("yesterday_rank",ro.getRankingYesterdayMin());
        qqw.lt("yesterday_rank",ro.getRankingYesterdayMax());
        qqw.gt("previous_rank",ro.getRankingPreviousDayMin());
        qqw.lt("previous_rank",ro.getRankingPreviousDayMax());

        if(null != ro.getFloatDaySort() && ro.getFloatDaySort() == 2){
            if(null != ro.getFloatDayMin()){
                qqw.gt("day_float",ro.getFloatDayMin());
            }
            if(null != ro.getFloatDayMax()){
                qqw.lt("day_float",ro.getFloatDayMax());
            }
            if(null != ro.getFloatDayMax() || null != ro.getFloatDayMin()){
                //大于0
                qqw.gt("day_float",0);
            }
        }else{
            if(null != ro.getFloatDayMax()){
                qqw.gt("day_float",-ro.getFloatDayMax());
            }
            if(null != ro.getFloatDayMin()){
                qqw.lt("day_float",-ro.getFloatDayMin());
            }
            //小于0
            if(null != ro.getFloatDayMax() || null != ro.getFloatDayMin()){
                qqw.lt("day_float",0);
            }
        }
        if(null != ro.getFloatYesterdaySort() && ro.getFloatYesterdaySort() == 2){
            if(null != ro.getFloatYesterdayMin()){
                qqw.gt("yesterday_float",ro.getFloatYesterdayMin());
            }
            if(null != ro.getFloatYesterdayMax()){
                qqw.lt("yesterday_float",ro.getFloatYesterdayMax());
            }
            //大于0
            if(null != ro.getFloatYesterdayMax() || null != ro.getFloatYesterdayMin()){
                qqw.gt("yesterday_float",0);
            }
        }else{
            if(null != ro.getFloatYesterdayMax()){
                qqw.gt("yesterday_float",-ro.getFloatYesterdayMax());
            }
            if(null != ro.getFloatYesterdayMin()){
                qqw.lt("yesterday_float",-ro.getFloatYesterdayMin());
            }
            //小于0
            if(null != ro.getFloatYesterdayMax() || null != ro.getFloatYesterdayMin()){
                qqw.lt("yesterday_float",0);
            }
        }
        if(null != ro.getFloatPreviousDaySort() && ro.getFloatPreviousDaySort() == 2){
            if(null != ro.getFloatPreviousDayMin()){
                qqw.gt("previous_float",ro.getFloatPreviousDayMin());
            }
            if(null != ro.getFloatPreviousDayMax()){
                qqw.lt("previous_float",ro.getFloatPreviousDayMax());
            }
            //大于0
            if(null != ro.getFloatPreviousDayMax() || null != ro.getFloatPreviousDayMin()){
                qqw.gt("previous_float",0);
            }
        }else{
            if(null != ro.getFloatPreviousDayMax()){
                qqw.gt("previous_float",-ro.getFloatPreviousDayMax());
            }
            if(null != ro.getFloatPreviousDayMin()){
                qqw.lt("previous_float",-ro.getFloatPreviousDayMin());
            }
            //小于0
            if(null != ro.getFloatPreviousDayMax() || null != ro.getFloatPreviousDayMin()){
                qqw.lt("previous_float",0);
            }
        }

        if(null != ro.getRankingDaySort() && ro.getRankingDaySort() == 2){
            qqw.orderByDesc("day_rank");
        }else{
            qqw.orderByAsc("day_rank");
        }

        Page<TAmazonUsa> three = mapper.selectPage(page, qqw);
//        List<TAmazonUsa> records = three.getRecords();
//
//        for(TAmazonUsa record : records){
//            String department =  DepartmentTypeEnum.getDescByValue(ro.getDepartment());
//            String redisKey = "trend:"+ department +":" + record.getSearchTerm();
//
//            String s = redisUtil.get(redisKey);
//            if(null != s){
//                List<TAmazonUsa> tAmazonUsa = JSON.parseArray(s, TAmazonUsa.class);
//                record.setSort(tAmazonUsa);
//            }
//        }
//        three.setRecords(records);
        return three;
    }

    @Override
    public List<TAmazonUsa> getRankingList(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());

        List<TAmazonUsa> TAmazonUsa = mapper.selectList(qqw);
        return TAmazonUsa;
    }
    @Override
    public Page<TAmazonUsa> getRankingListTask(Page<TAmazonUsa> page, InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());
        qqw.orderByAsc("date");
        Page<TAmazonUsa> three = mapper.selectPage(page, qqw);
        return three;
    }

    @Override
    public Integer getCount(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
        qqw.ge("date",s);
        qqw.le("date",e);
        Integer integer = mapper.selectCount(qqw);
        return integer;
    }

    @Override
    public List<TAmazonUsa> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String s = sdf2.format(ro.getSDate());
        String e = sdf2.format(ro.getEDate());
        ExcludeEmptyQueryWrapper<TAmazonUsa> qqw = new ExcludeEmptyQueryWrapper();
//        qqw.ge("date",s);
//        qqw.le("date",e);
        qqw.eq("search_term",ro.getSearchTerm());
        qqw.orderByAsc("date");
        List<TAmazonUsa> three = mapper.selectList(qqw);
        return three;
    }
}
