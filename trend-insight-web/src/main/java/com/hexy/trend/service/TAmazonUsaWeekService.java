package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonUsaMonth;
import com.hexy.trend.entity.TAmazonUsaWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonUsaWeekService extends IService<TAmazonUsaWeek> {
    public List<TAmazonUsaWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonUsaWeek> list) ;

    public void updateBatch(List<TAmazonUsaWeek> list);
    public Page<TAmazonUsaWeek> getRankingList(Page<TAmazonUsaWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonUsaWeek> getRankingListTask(Page<TAmazonUsaWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonUsaWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
