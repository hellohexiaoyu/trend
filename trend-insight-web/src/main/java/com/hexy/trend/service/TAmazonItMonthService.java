package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonFrWeek;
import com.hexy.trend.entity.TAmazonItMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonItMonthService extends IService<TAmazonItMonth> {
    public List<TAmazonItMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonItMonth> list) ;

    public void updateBatch(List<TAmazonItMonth> list);
    public Page<TAmazonItMonth> getRankingList(Page<TAmazonItMonth> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonItMonth> getRankingListTask(Page<TAmazonItMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonItMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);

}
