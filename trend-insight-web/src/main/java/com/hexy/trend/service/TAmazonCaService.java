package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonMx;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-10-13
 */
public interface TAmazonCaService extends IService<TAmazonCa> {
    public List<TAmazonCa> getAmazonCaList(Date date);

    public void addBatch(List<TAmazonCa> list) ;

    public void updateBatch(List<TAmazonCa> list);
    public List<TAmazonCa> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
    public Page<TAmazonCa> getRankingList(Page<TAmazonCa> page, InsightRO.GetInsightRO ro);
    public Page<TAmazonCa> getRankingListTask(Page<TAmazonCa> page,InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);
}
