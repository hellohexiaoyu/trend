package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonEsMonth;
import com.hexy.trend.entity.TAmazonEsWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonEsWeekService extends IService<TAmazonEsWeek> {
    public List<TAmazonEsWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonEsWeek> list) ;

    public void updateBatch(List<TAmazonEsWeek> list);
    public Page<TAmazonEsWeek> getRankingList(Page<TAmazonEsWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonEsWeek> getRankingListTask(Page<TAmazonEsWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonEsWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
