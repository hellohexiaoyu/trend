package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonCaWeek;
import com.hexy.trend.entity.TAmazonDeMonth;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonDeMonthService extends IService<TAmazonDeMonth> {
    public List<TAmazonDeMonth> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonDeMonth> list) ;

    public void updateBatch(List<TAmazonDeMonth> list);
    public Page<TAmazonDeMonth> getRankingList(Page<TAmazonDeMonth> page, InsightRO.GetInsightRO ro);

    public Page<TAmazonDeMonth> getRankingListTask(Page<TAmazonDeMonth> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);



    public List<TAmazonDeMonth> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
