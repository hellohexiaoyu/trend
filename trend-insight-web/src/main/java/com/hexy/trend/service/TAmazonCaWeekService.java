package com.hexy.trend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hexy.trend.bean.aro.InsightRO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.entity.TAmazonCaMonth;
import com.hexy.trend.entity.TAmazonCaWeek;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hexy
 * @since 2023-11-20
 */
public interface TAmazonCaWeekService extends IService<TAmazonCaWeek> {
    public List<TAmazonCaWeek> getAmazonCaList(Date date);
    public void addBatch(List<TAmazonCaWeek> list) ;

    public void updateBatch(List<TAmazonCaWeek> list);
    public Page<TAmazonCaWeek> getRankingList(Page<TAmazonCaWeek> page, InsightRO.GetInsightRO ro);


    public Page<TAmazonCaWeek> getRankingListTask(Page<TAmazonCaWeek> page, InsightRO.GetInsightDepartmentRO ro);

    Integer getCount(InsightRO.GetInsightDepartmentRO ro);


    public List<TAmazonCaWeek> getRankingListForQS(InsightRO.GetInsightDepartmentRO ro);
}
