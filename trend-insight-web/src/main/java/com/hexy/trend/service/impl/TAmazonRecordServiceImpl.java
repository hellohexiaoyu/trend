package com.hexy.trend.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hexy.trend.dao.TAmazonRecordMapper;
import com.hexy.trend.entity.TAmazonMx;
import com.hexy.trend.entity.TAmazonRecord;
import com.hexy.trend.mybatis.converter.ExcludeEmptyQueryWrapper;
import com.hexy.trend.service.TAmazonRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 更新记录表 服务实现类
 * </p>
 *
 * @author hexy
 * @since 2023-11-02
 */
@Service
public class TAmazonRecordServiceImpl extends ServiceImpl<TAmazonRecordMapper, TAmazonRecord> implements TAmazonRecordService {
    @Autowired
    private TAmazonRecordMapper mapper;
    @Override
    public Integer saveRecord(TAmazonRecord record){
        record.setCreatedAt(new Date());
        int insert = mapper.insert(record);
        return insert;
    }

    @Override
    public TAmazonRecord getRecordByDepartment(String department,Integer type) {
        ExcludeEmptyQueryWrapper<TAmazonRecord> qqw = new ExcludeEmptyQueryWrapper();
        qqw.eq("department",department);
        qqw.eq("type",type);
        qqw.orderByDesc("created_at");
        qqw.last("LIMIT 1");
        TAmazonRecord record = mapper.selectOne(qqw);
        return record;
    }
}
