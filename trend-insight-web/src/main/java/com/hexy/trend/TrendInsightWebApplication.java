package com.hexy.trend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.TimeZone;

@ComponentScan(basePackages = "com.hexy")
@SpringBootApplication
@MapperScan(value = "com.hexy.*.dao")
@EnableAsync
@EnableScheduling
public class TrendInsightWebApplication {

    public static void main(String[] args) {
//        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
        SpringApplication.run(TrendInsightWebApplication.class, args);
    }

}
