package com.hexy.trend.bean.aro;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hexy.trend.web.ro.BasicPagerRO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;


public class UserRO {

    @Data
    @ApiModel
    @Validated
    public static class AddUserRO{

        @NotBlank
        @ApiModelProperty(value = "用户名")
        private String username;
        @NotBlank
        @ApiModelProperty(value = "密码")
        private String password;

        @NotBlank
        @ApiModelProperty(value = "确认密码")
        private String confirmPassword;

        @NotBlank
        @ApiModelProperty(value = "验证码")
        private String code;

        @ApiModelProperty(value = "邮箱")
        private String email;

        @ApiModelProperty(value = "年龄")
        private Integer age;

        @ApiModelProperty(value = "性别(0:女 1男)")
        private Integer sex;

        @ApiModelProperty(value = "电话")
        private String tel;

        @ApiModelProperty(value = "地址")
        private String addr;

        @ApiModelProperty(value = "身份证号")
        private String card;

        @ApiModelProperty(value = "头像")
        private String avatar;
        @ApiModelProperty(value = "账户类型（0：管理员 1：普通用户 2：vip）")
        private Integer type;
        @ApiModelProperty(value = "开始时间")
        private Date startTime;
        @ApiModelProperty(value = "到期时间")
        private Date endTime;

    }
    @Data
    @ApiModel
    @Validated
    public static class SendMailRO{
        @NotBlank
        @ApiModelProperty(value = "邮箱")
        private String email;
    }

    @Data
    @ApiModel
    @Validated
    public static class CheckUserRO{

        @NotBlank
        @ApiModelProperty(value = "用户名")
        private String username;
    }

    @Data
    @ApiModel
    @Validated
    public static class GetUserListRO extends BasicPagerRO {


        @ApiModelProperty(value = "用户名")
        private String userName;

        @ApiModelProperty(value = "0:管理员 1：普通 2：VIP")
        private Integer type;
        @ApiModelProperty(value = "开始日期")
        private Date startTime;

        @ApiModelProperty(value = "结束日期")
        private Date endTime;
    }

    @Data
    @ApiModel
    @Validated
    public static class SetPwRO{

        @NotBlank
        @ApiModelProperty(value = "密码")
        private String password;
        @NotBlank
        @ApiModelProperty(value = "确认密码")
        private String confirmPassword;
    }

    @Data
    @ApiModel
    @Validated
    public static class SetUserVipRO{
        @NotNull
        @ApiModelProperty(value = "用户ID")
        private Integer id;

        @NotNull
        @ApiModelProperty(value = "开始日期")
        private Date startTime;
        @NotNull
        @ApiModelProperty(value = "结束日期")
        private Date endTime;
    }
}
