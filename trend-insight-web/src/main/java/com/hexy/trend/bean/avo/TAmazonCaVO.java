package com.hexy.trend.bean.avo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hexy.trend.entity.TAmazonCa;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@ApiModel("数据测试")
public class TAmazonCaVO {

    private Integer id;

    private String department;

    private String searchTerm;

    private Long dayRank;

    private Long dayFloat;

    private Long yesterdayRank;

    private Long yesterdayFloat;

    private Long previousRank;

    private Long previousFloat;
    @ApiModelProperty(value="数据产生日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;
    private Date createdAt;
    private Date updatedAt;

    private List<TAmazonCa> sort;
}
