package com.hexy.trend.bean;

import lombok.Data;

@Data
public class InsightProductRanking {
    String department;
    String search_term;
    Long search_frequency_rank;
}
