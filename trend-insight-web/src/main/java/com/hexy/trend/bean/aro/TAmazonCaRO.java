package com.hexy.trend.bean.aro;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@ApiModel("数据测试")
public class TAmazonCaRO {

    private Integer id;

    private String department;

    private String searchTerm;
    private String batchNum;

    private Long searchFrequencyRank;

    private Date date;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;
}
