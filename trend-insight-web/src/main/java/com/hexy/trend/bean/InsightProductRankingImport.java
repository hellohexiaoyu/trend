package com.hexy.trend.bean;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel
@Data
public class InsightProductRankingImport {
    @ExcelProperty(value = "部门",index = 0)
    @ColumnWidth(13)
    @ApiModelProperty(value="部门")
//    @ExcelValid(message = "列【处方ID】为必输项")
    String department;
    @ExcelProperty(value = "关键词",index = 1)
    @ColumnWidth(13)
    @ApiModelProperty(value="关键词")
    String searchTerm;

    @ExcelProperty(value = "排名",index = 2)
    @ColumnWidth(13)
    @ApiModelProperty(value="排名")
    Long dayRank;

    @ExcelProperty(value = "中文表述",index = 3)
    @ColumnWidth(13)
    @ApiModelProperty(value="中文表述")
    String translate;
}
