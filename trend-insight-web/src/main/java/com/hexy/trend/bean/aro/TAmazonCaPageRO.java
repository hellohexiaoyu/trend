package com.hexy.trend.bean.aro;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@ApiModel("数据测试")
public class TAmazonCaPageRO {
    @ApiModelProperty(value = "页码", required = true, example = "1")
    private Integer pageNo = 1;

    @ApiModelProperty(value = "页行数", required = true, example = "20")
    private Integer pageSize = 20;
    private Integer id;

    private String department;

    private String searchTerm;
    private String batchNum;

    private Long searchFrequencyRank;

    private Date date;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdAt;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedAt;

    @ApiModelProperty(value = "开始时间", example = "1")
    private Long startTime;

    @ApiModelProperty(value = "结束时间", example = "10000000")
    private Long endTime;
}
