package com.hexy.trend.bean.aci;

import com.hexy.trend.bean.aro.UserRO;
import com.hexy.trend.entity.TAmazonUser;
import com.hexy.trend.security.UserDetails;
import com.hexy.trend.web.converter.ConverterWorker;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = ConverterWorker.class)
public interface UserCI {

    UserCI INSTANCE = Mappers.getMapper(UserCI.class);

    UserDetails convert(TAmazonUser user);
    TAmazonUser convert(UserRO.AddUserRO user);

}
