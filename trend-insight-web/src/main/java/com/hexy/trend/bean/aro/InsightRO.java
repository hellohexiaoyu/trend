package com.hexy.trend.bean.aro;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hexy.trend.web.ro.BasicPagerRO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class InsightRO {

    @Data
    @ApiModel
    @Validated
    public static class GetInsightRO extends BasicPagerRO {

        @ApiModelProperty(value = "搜索词", required = true)
        private String searchTerm;
        @NotNull
        private Integer department;
        @ApiModelProperty(value = "日期", required = true)
        @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
        private Date date;
        @ApiModelProperty(value = "当日排名最小值")
        private Long rankingDayMin;
        @ApiModelProperty(value = "当日排名最大值")
        private Long rankingDayMax;
        @ApiModelProperty(value = "昨日排名最小值")
        private Long rankingYesterdayMin;
        @ApiModelProperty(value = "昨日排名最大值")
        private Long rankingYesterdayMax;
        @ApiModelProperty(value = "前日排名最小值")
        private Long rankingPreviousDayMin;
        @ApiModelProperty(value = "前日排名最大值")
        private Long rankingPreviousDayMax;

        @ApiModelProperty(value = "当日涨幅最小值")
        private Long floatDayMin;
        @ApiModelProperty(value = "当日涨幅最大值")
        private Long floatDayMax;
        @ApiModelProperty(value = "昨日涨幅最小值")
        private Long floatYesterdayMin;
        @ApiModelProperty(value = "昨日涨幅最大值")
        private Long floatYesterdayMax;

        @ApiModelProperty(value = "前日涨幅最小值")
        private Long floatPreviousDayMin;
        @ApiModelProperty(value = "前日涨幅最大值")
        private Long floatPreviousDayMax;

        @NotNull
        @ApiModelProperty(value = "当日数据排序1：升序 2：降序")
        private Integer rankingDaySort;

        @NotNull
        @ApiModelProperty(value = "今日涨幅1：升序 2：降序")
        private Integer floatDaySort;

        @NotNull
        @ApiModelProperty(value = "昨日涨幅1：升序 2：降序")
        private Integer floatYesterdaySort;

        @NotNull
        @ApiModelProperty(value = "前日涨幅1：升序 2：降序")
        private Integer floatPreviousDaySort;
    }

    @Data
    @ApiModel
    @Validated
    public static class GetInsightDepartmentRO {
        @ApiModelProperty(value = "搜素词", required = true)
        private String searchTerm;
        @ApiModelProperty(value = "开始日期", required = true)
        private Date sDate;

        @ApiModelProperty(value = "结束日期", required = true)
        private Date eDate;
    }

    @Data
    @ApiModel
    @Validated
    public static class GetRecordRO {
        @ApiModelProperty(value = "部门", required = true)
        @NotNull
        private Integer department;

        @ApiModelProperty(value = "标识（1：天，2：周，3：月）", required = true)
        @NotNull
        private Integer type;

    }
    @Data
    @ApiModel
    @Validated
    public static class GetSortRO {

        @NotBlank
        @ApiModelProperty(value = "搜索词", required = true)
        private String searchTerm;
        @NotNull
        @ApiModelProperty(value = "部门", required = true)
        private Integer department;
    }
}
