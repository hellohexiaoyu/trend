package com.hexy.trend.bean.aci;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hexy.trend.bean.avo.TAmazonCaVO;
import com.hexy.trend.entity.TAmazonCa;
import com.hexy.trend.web.converter.ConverterWorker;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ConverterWorker.class)
public interface InsightCI {

    InsightCI INSTANCE = Mappers.getMapper(InsightCI.class);
//    @Mapping(target = "searchFrequencyRank", source = "dayRank")
    TAmazonCa convert(TAmazonCa list);
    List<TAmazonCa> convert(List<TAmazonCa> list);

}
